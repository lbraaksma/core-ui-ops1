The |project| can be configured by the following steps:

* **Step 1.** Add configuration file config.json
* **Step 2.** Add navigation file navigation.json
* **Step 3.** Make the Service available to the proxy server

**Step 1.**

Add configuration file :file:`config.json` to the folder ``/var/www/conf`` of the |project| container.
In our docker-compose example we can simply put this file in the volume ``/myhost/docker-volumes/core-ui/conf`` on the host.
In this file you need to specify the following parameters:

* endpoint URL of the AC authentication web service;
* landingpage URL for a AC Service that will be used as the landing page;
* a key name for the cookie token.

A :file:`config.json` file for the Core UI should look like:


.. code-block :: console

   {
     "endpoints": {
      "authenticate": "/api/v1/authenticate"
     },
     "url": {
     "headerIframeURL": "/header",
     "authServiceURL": "/auth",
     "landingPageURL": "/ops360",
     "parentKey": "parent",
     "originKey": "origin"
     },
     "cookie": {
     "tokenKey": "rights",
      "tokenUnauthorizedValue": "unauthorized"
     }
   }


**Step 2.**

Add navigation file :file:`navigation.json` to the folder ``/var/www/conf`` of the |project| container.
In our docker-compose example we can also store this file in the mounted volume ``/myhost/docker-volumes/core-ui/conf`` on the host.
When a new AC Service is installed this file needs to be updated if it contains a UI.
This file contains the URL's of all AC Services that will be displayed in the menu items of the |project|.
For every AC Web Service you need to specify:

* title, title in the topmenu and submenu;
* service, the authentication roles giving the permission to use the service;
* url, the URL where the service is located.

The authentication roles are roles that define the authentication for a particular service.
If a user is not authenticated for this service role, the user can not choose this service from the menu.

For example when the AC Data Browsing utility is added the :file:`navigation.json` file could look like:


.. code-block:: console

   [
    {
       "title": "Data Browsing",
       "pages": [
         {
           "title": "Browse AC-data",
           "service": ["AUTHENTICATION.ALL", "BROWSE.ALL"],
           "url": "/browse/ac/search"
         },
         {
           "title": "Browse ACX-data",
           "service": ["AUTHENTICATION.ALL"],
           "url": "/browse/acx/search"
         }

       ]
     }
   ]


The |project| uses this file to add topmenu- and submenu-items to the header menu.

.. figure:: /img/ac_core_ui_header_01.png
   :alt: core ui header with databrowsing service
   :align: center

   Header with menu and sub menu items.

In the Admin guides of the AC Services you can find which menu-items to add to the |project| by updating the :file:`navigation.json`.

**Step 3.**

Make the Service available to a proxy service by specifying:

.. code-block:: console

   proxy all / to core-ui:80
   proxy all /*/config.json to core-ui:80
   proxy all /*/navigation.json to core-ui:80

As an example, when a NGINX server is used as a proxy server, the configuration file needs to be updated.
The configuration file of a NGINX server can be found in the folder ``/etc/nginx/available-sites``.
The following lines need to be added to this file:

.. code-block:: console

   location / {
        proxy_pass http://core-ui:80;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $http_host;
        proxy_set_header X-Forwarded-Proto https;
        proxy_redirect off;
        proxy_http_version 1.1;

        access_log  /var/log/nginx/access.log  upstream_logging;
   }

   location ~* \*\config.json {
        proxy_pass http://core-ui:80;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $http_host;
        proxy_set_header X-Forwarded-Proto https;
        proxy_redirect off;
        proxy_http_version 1.1;

        access_log  /var/log/nginx/access.log  upstream_logging;
   }

   location ~* \*\navigation.json {
        proxy_pass http://core-ui:80;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $http_host;
        proxy_set_header X-Forwarded-Proto https;
        proxy_redirect off;
        proxy_http_version 1.1;

        access_log  /var/log/nginx/access.log  upstream_logging;
   }