*****************************
Minimal Software Requirements
*****************************

* Docker
* AC Authentication WebService
* Proxy service (e.g. a separate NGNIX container)

All software requirements must be met prior to installing the AC Core UI service. If not, please resolve this first.

******************************
Installing the Core UI Service
******************************

The |project| is provided as a Docker image file which you can download from the Asset Control Userweb as follows:

   - Log onto the Asset Control `Userweb <https://services.asset-control.com/userweb>`_. Login
     credentials are required to access this site.
   - In the “AC Plus Applications” section, follow the link for “AC Core UI".
   - Click the link in the "Application builds" section to download the package file.

The docker image file for |project| is contained in a `*.tar.gz` archive.
There is no need to unzip the package just load the Docker image file to your local Docker repository.
For example let's say the image name is core-ui:1.0.0 you can use the command:


.. code-block:: console

   docker load -i <core-ui-1.0.0-imagefile>.tar.gz

In this Administration Guide we will use the docker-compose utility.

The |project| can be configured using configuration files. These files are stored in the folder ``/var/www/conf`` inside the container.
In this Administration Guide this directory will be bind mounted to a directory (volume) on the host machine.
With the ``volumes`` parameter in a docker-compose file you can do this mount.
A docker-compose file,  :file:`docker-compose.yml` would look like:


.. code-block:: console

   image: "localdockerrepo:5000/core-ui:1.0.0"
   container_name: core-ui
   hostname: core-ui
   expose:
     - "80"
   volumes:
     - "/myhost/docker-volumes/core-ui/conf:/var/www/conf"
   networks:
     - ac-net

   networks:
     ac-net:
       external: true


This docker-compose file declares a local folder on the host ``/myhost/docker-volumes/core-ui/conf`` to be Docker volume that is bind mounted to the folder ``/var/www/conf`` inside the container.
Configuration files can now be added to this volume.

In this example containers are configured to use a virtual docker `ac-net` network.

Port 80 will be exposed within this virtual `ac-net` network for other AC Services to access the |project|.

*****************************
Upgrading the Core UI Service
*****************************

Stop and remove the container (when using docker-compose) but do not remove the volume.
Run the following command from the folder where your docker-compose file resides:

.. code-block:: console

   docker-compose down


Download the new image from `Userweb <https://services.asset-control.com/userweb>`.
Load the new image to your local Docker repository.
Update the docker-compose file with the new image.


*****************************
Uninstall the Core UI Service
*****************************

To stop and remove the container and remove volume (when using docker-compose).
Run the following command from the folder where your docker-compose file resides:


.. code-block:: console

   docker-compose down --volumes


It is not common practice to remove the image as the old image can be kept without problem.
Look for the `image id`  with command:

.. code-block:: console

   docker images

However if you wish to remove the image the command is:


.. code-block:: console

   rmi imageid

If you have made changes to the configuration file of a proxy server for the |project| do not forget to delete them as well.