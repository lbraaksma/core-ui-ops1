###################
 What is |project|?
###################

|project| belongs to the new collection of Asset Control Services such as Data Cleansing, Data Browsing, Authentication Service and the Business Domain Model Service.
They are developed based on the MicroService architecture.

|project| is the foundational UI layer that provides the core UI functionality common to all Asset Control Web Services.
It assembles seamlessly separate Web UIs under a unified header menu.

|project| provides the following services:

- Login and authentication services
  All Asset Control Services user login and authentication are handled by the |project| component. Authentication tracking is done via cookies.
  For the authentication to work you need to install AC Authentication Service, this is a separate AC Service.

- Header Menu
  The Header Menu provides a single launching point for all AC Services.


#################
How does it work?
#################

|project| is responsible for the user logon procedure for all AC Services.
The AC Authentication Service is used for authorising and authenticating the user.
|project| uses cookies and it messages decoded rights to other AC Services enabling them to verify the rights of users.

Once a user has successfully logged in (i.e. authenticated and authorized) |project| displays the header menu of the AC Web UI.
From the header menu of the AC Web UI a user can navigate to the AC Services.
But the header menu will only display those services he is authorized to use.
You can configure one AC Service as the default application.

|project| is installed as a docker image. The docker image contains an NGINX server which will act as a web server. When you run the container,
it launches a NGINX web server to render the |project| Page.

############################
AC MicroService Architecture
############################

A MicroServices architecture is a collection of independently deploy-able services.
Every service has its own function and will be deployed in its own Docker container.
Every container has its own isolated workload environment making it independently deploy-able and scalable.
Each individual AC Service runs in its own isolated environment, separate from the others within the architecture.
Some of the new AC Services are Web applications having their own Web User Interfaces (UI). They are developed to
provide the the ability to configure a single unified Web UI (AC Web UI).
The AC Core UI Service is the core component of the AC Services representing the common properties of this UI.