****************************
Starting the Core UI Service
****************************

Start the container using the docker-compose command from the folder where your docker-compose file resides:

.. code-block:: console

   docker-compose up -d


Once |project| is started, point your web browser to the landing page URL specified in the config.json file to launch the GUI.

****************************
Stopping the Core UI Service
****************************

Stop the container using the docker-compose command from the folder where your docker-compose file resides:

.. code-block:: console

   docker-compose stop

This will stop the container and the landing page URL is not reachable.