# -*- coding: utf-8 -*-
import acdocutils

project = 'AC Core UI Service'
version = '1.0'
release   = acdocutils.get_release_version(version)

# Chapter number prefix text. Default is ''. Uncomment this to override.
chapter_text = 'Section'

latex_title = u'%s \\textsuperscript{\\texttrademark{}} %s' % (project, version)

pn = "ac-core-ui-ops-doc"

# document subtitle, use 'author' tag to pass in
# Include latex size/formatting commands as needed
ag = 'Administration Guide'
dg = 'Development Guide'
rn = 'Release Notes'

latex_documents = [
  ('ag/index', '%s-AG.tex' % pn, latex_title, ag, 'ac-guide'),
  ('rn/index', '%s-RN.tex' % pn, latex_title, rn, 'ac-guide')
]

extensions = [
#    'ac_ref',
    'ac_appendix',
    'ac_table',
    'acprop',
    'acattr',
    'acerr',
    'acfn',
    'acobj',
    'acop',
    'acprg',
    'actyp',
    'acapiattr',
    'acapiobj']

project_replacements = {
    # Name as known on Userweb
    'product': 'AC CORE UI OPS',
    # (downloaded package name, with extension variations)
    'package': 'ac-core-ui-ops-%s-pkg' % release,
    'downloaded_package': 'ac-core-ui-ops--%s-pkg.tar.gz' % release,
    # Userweb section: 'AC Plus Applications', 'AC Plus Interfaces' or 'AC Plus Normalized and Consolidated Data Models'
    'section': 'AC Plus Applications',
}

publish_to = ['webdoc','Applications/%s %s' % (project, version)]
