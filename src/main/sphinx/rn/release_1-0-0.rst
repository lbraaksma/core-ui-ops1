######################
1.0.0 (February 2020)
######################

First external release

*******
Summary
*******

|rn-new|     {n} `New`_

|rn-fixed|   {n} `Fixed`_

|rn-updated| {n} `Updated`_

.. Optional feature list

* {feature 1}

* {feature 2}

* ...

* {feature N}

.. tabularcolumns:: |X[c]|X[c]|X|X[10]|

.. csv-table:: Summary
   :header-rows: 1
   :class: small

   Type,         Ref,        AC Service Desk, Description
   |rn-new|,     `XXX-001`_,                , :ref:`XXX-001`
   |rn-new|,     `XXX-002`_,                , :ref:`XXX-002`
   |rn-fixed|,   `XXX-003`_,                , :ref:`XXX-003`
   |rn-fixed|,   `XXX-004`_,                , :ref:`XXX-004`
   |rn-updated|, `XXX-005`_,                , :ref:`XXX-005`
   |rn-updated|, `XXX-006`_,                , :ref:`XXX-006`

************
Known Issues
************

None.
