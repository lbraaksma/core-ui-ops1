\vskip -40pt
\babel@toc {english}{}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}What is AC CORE UI OPS?}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}How does it work?}{1}{section.1.2}
\contentsline {chapter}{\numberline {2}Installation}{2}{chapter.2}
\contentsline {section}{\numberline {2.1}Setup}{2}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Requirements}{2}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}AC Server}{2}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}Configuring the KIT}{2}{subsection.2.1.3}
\contentsline {subsubsection}{\numberline {2.1.3.1}Environment variables for UI Kit}{2}{subsubsection.2.1.3.1}
\contentsline {subsubsection}{\numberline {2.1.3.2}Starting the Core UI Service}{3}{subsubsection.2.1.3.2}
