\vskip -40pt
\babel@toc {english}{}
\contentsline {chapter}{\numberline {1}1.0.0 (September 2019)}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Known issues}{1}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Authentication}{1}{subsection.1.1.1}
\contentsline {chapter}{\numberline {2}1.0.1 (October 2019)}{2}{chapter.2}
\contentsline {section}{\numberline {2.1}Summary}{2}{section.2.1}
\contentsline {section}{\numberline {2.2}Install Notes}{2}{section.2.2}
\contentsline {section}{\numberline {2.3}Known issues}{2}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Authentication}{2}{subsection.2.3.1}
\contentsline {section}{\numberline {2.4}New}{2}{section.2.4}
\contentsline {section}{\numberline {2.5}Fixed}{2}{section.2.5}
\contentsline {subsection}{\numberline {2.5.1}Cassandra WriteTimeoutException on INSERT}{2}{subsection.2.5.1}
\contentsline {section}{\numberline {2.6}Updated}{2}{section.2.6}
\contentsline {chapter}{\numberline {3}1.0.2 (October 2019)}{3}{chapter.3}
\contentsline {section}{\numberline {3.1}Summary}{3}{section.3.1}
\contentsline {section}{\numberline {3.2}Install Notes}{3}{section.3.2}
\contentsline {section}{\numberline {3.3}Known issues}{3}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Authentication}{3}{subsection.3.3.1}
\contentsline {section}{\numberline {3.4}New}{3}{section.3.4}
\contentsline {section}{\numberline {3.5}Fixed}{3}{section.3.5}
\contentsline {subsection}{\numberline {3.5.1}Ingestion Service validation of large JSON data is very slow}{3}{subsection.3.5.1}
\contentsline {subsection}{\numberline {3.5.2}Configurable Cassandra reconnection policy}{4}{subsection.3.5.2}
\contentsline {subsection}{\numberline {3.5.3}Retry Cassandra connection on startup}{4}{subsection.3.5.3}
\contentsline {subsection}{\numberline {3.5.4}Default MAX\_RETRIES changed for Propagation Service}{4}{subsection.3.5.4}
\contentsline {subsection}{\numberline {3.5.5}Propagation Service should not retry in all cases}{4}{subsection.3.5.5}
\contentsline {subsection}{\numberline {3.5.6}Failure directory configurable for Propagation Service}{4}{subsection.3.5.6}
\contentsline {section}{\numberline {3.6}Updated}{4}{section.3.6}
\contentsline {subsection}{\numberline {3.6.1}Docker image size significantly reduced}{4}{subsection.3.6.1}
\contentsline {subsection}{\numberline {3.6.2}HTTPS supported}{4}{subsection.3.6.2}
\contentsline {subsection}{\numberline {3.6.3}New lineage property \sphinxstyleliteralintitle {userId} supported}{5}{subsection.3.6.3}
\contentsline {chapter}{\numberline {4}1.0.3 (November 2019)}{6}{chapter.4}
\contentsline {section}{\numberline {4.1}Summary}{6}{section.4.1}
\contentsline {section}{\numberline {4.2}Install Notes}{6}{section.4.2}
\contentsline {section}{\numberline {4.3}Known issues}{6}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Authentication}{6}{subsection.4.3.1}
\contentsline {section}{\numberline {4.4}New}{7}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Support data lineage for correction values}{7}{subsection.4.4.1}
\contentsline {section}{\numberline {4.5}Fixed}{7}{section.4.5}
\contentsline {section}{\numberline {4.6}Updated}{7}{section.4.6}
\contentsline {subsection}{\numberline {4.6.1}Include function return value in lineage}{7}{subsection.4.6.1}
\contentsline {chapter}{\numberline {5}1.0.4 (TBD)}{8}{chapter.5}
\contentsline {section}{\numberline {5.1}Summary}{8}{section.5.1}
\contentsline {section}{\numberline {5.2}Install Notes}{8}{section.5.2}
\contentsline {section}{\numberline {5.3}Known issues}{8}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Authentication}{8}{subsection.5.3.1}
\contentsline {section}{\numberline {5.4}New}{8}{section.5.4}
\contentsline {subsection}{\numberline {5.4.1}Support caused/causedBy relations between attribute value changes}{8}{subsection.5.4.1}
\contentsline {section}{\numberline {5.5}Fixed}{9}{section.5.5}
\contentsline {section}{\numberline {5.6}Updated}{9}{section.5.6}
