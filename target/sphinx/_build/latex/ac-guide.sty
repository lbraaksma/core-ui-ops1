% This specifies various PDF style features:
%
% - Colour definitions
% - Size of font in code blocks
% - How links look
% - Style of section headers
% - Per-page headers & footers
%
\NeedsTeXFormat{LaTeX2e}
\ProvidesPackage{ac-guide}[2016/01/21 AC Technical Documentation]

\usepackage{hyperref}
\usepackage{etoolbox}
\usepackage{xcolor}
\usepackage{colortbl}
\usepackage[Sonny]{fncychap}
\usepackage{fancyhdr}
\usepackage{pdflscape} % Allows landscape pages (For viewing)
\usepackage[breakwords,fit]{truncate} % To truncate long titles in footer
\usepackage[document]{ragged2e} % Ragged right justification
\usepackage{tabu}
\usepackage{longtable}
\usepackage{calc}
\usepackage{tcolorbox}
\renewcommand{\TruncateMarker}{\textsf{...}}

% \usepackage{lscape} % Allows landscape pages (For printing)
% \usepackage[table]{xcolor}
% \usepackage{lastpage} % For last page number in 'Page N of M'

%% COLOURS
% These are the default link colours specified in sphinx.sty
\definecolor{TitleColor}{rgb}{0.137,0.122,0.125} % PANTONE BLACK, RGB 35,31,32
\definecolor{InnerLinkColor}{rgb}{0.0,0.255,0.416} % PANTONE 7694, RGB 0,65,106
\definecolor{OuterLinkColor}{rgb}{0.0,0.255,0.416} % PANTONE 7694, RGB 0,65,106
% AC colours - Taken from selection available in AC Brand Guidelines March 2016
\definecolor{acorange}{RGB}{225,107,0}  % PANTONE 1505
\definecolor{acblue}{RGB}{0,65,106}     % PANTONE 7694
\definecolor{acred}{RGB}{157,22,46}     % PANTONE 7427
\definecolor{acgreen}{RGB}{0,148,130}   % PANTONE 3285
\definecolor{acpink}{RGB}{221,35,151}        % PANTONE PINK C
\definecolor{aclightblue}{RGB}{135,218,223}  % PANTONE 318
\definecolor{accaution}{RGB}{218,165,32}     % html goldenrod
% Table row colours
\definecolor{header}{RGB}{208,216,232}
\definecolor{evenrow}{RGB}{233,237,244}
\definecolor{oddrow}{RGB}{208,216,232}
% Code blocks and inline code
\definecolor{VerbatimColor}{RGB}{245,245,245} %% code-block background colour
\providecommand{\code}{}
% code-block text size
%\renewcommand{\code}[1]{{\small\texttt{#1}}} % Leave with normal defaults

% For docutils' parsed-literal which uses the sphinxalltt environment.
% Try to match code-block style defined above. Close but not identical.
% parsed-literal is rarely used so this should be enough for now.
\let\oldalltt\alltt
\let\endoldalltt\endalltt
\renewenvironment{alltt}{\oldalltt\begin{tcolorbox}[colback=gray!7,width=\dimexpr\textwidth\relax,boxrule=0.5pt,arc=0pt,left=0pt,right=0pt,top=0pt,bottom=0pt]}{\end{tcolorbox}\endoldalltt}

% For when table header color contrast is needed
\newcommand\WhiteText[1]{
   \textcolor{white}{#1}
}

%% Table of Contents (TOC)
% TOC links for title, page and dots between.
% Numbers in PDF bookmarks.

\hypersetup{%
linktoc=all,
colorlinks=true,
% linkcolor={\color{acblue}}
% hidelinks,
bookmarksnumbered
}

% Patch to add links to TOC dots.
% \makeatletter
% \pretocmd{\contentsline}
%   {\patchcmd{\@dottedtocline}
%      {\leaders}
%      {\hyper@linkstart{link}{#4}\leaders}
%      {}
%      {}%
%    \patchcmd{\@dottedtocline}
%      {\hfill}
%      {\hfill\hyper@linkend}
%      {}
%      {}}
%   {}
%   {}
% \makeatother

%% Custom admonition styles
\sphinxsetup{
     noteborder=1.5pt,      noteBorderColor={named}{acblue},
      tipborder=1.5pt,       tipBorderColor={named}{acgreen},
     hintborder=1.5pt,      hintBorderColor={named}{acpink},
attentionborder=1.5pt, attentionBorderColor={named}{acorange},
importantborder=1.5pt, importantBorderColor={named}{acorange},
  cautionborder=1.5pt,   cautionBorderColor={named}{acorange},
  warningborder=1.5pt,   warningBorderColor={named}{acred},
   dangerborder=1.5pt,    dangerBorderColor={named}{red},
    errorborder=1.5pt,     errorBorderColor={named}{acred},
     shadowsize=1.5pt
}

\renewcommand{\sphinxcrossref}[1]{{#1}} % Don't make envvar et al links italics


%% Macros for 'Sonny' fncychap style.
\ChNameVar{\flushright\normalsize} %% style for section name
\ChNumVar{\Huge} %% style for section digit number
\ChTitleVar{\Huge\flushleft} %% style for section title
\ChNameUpperCase %% Make section name uppercase

%% Layout of section, title, rules
%% \def\MaxLevel{3}
%% How to render the section word label and number
\renewcommand{\DOCH} {
    \CNV
    \FmN{\@chapapp}
    \CNoV
    \thechapter
}
\renewcommand{\DOTI}[1]{
    \CTV
    \FmTi{#1}
    \mghrulefill{0.4pt}
    \vskip 20\p@ %% Gap below title
}
\renewcommand{\DOTIS}[1]{
    \CTV
    \FmTi{#1}
    \mghrulefill{0.4pt}
    \vskip 20\p@ %% Gap below title
}

% Define headers/footers
\@ifundefined{fancyhf}{} {
   \fancypagestyle{normal} { % Other pages
     \fancyhf{}
     \fancyhead[L]{\includegraphics[width=3cm]{\acLogoMain}}
     \fancyhead[R]{\leftmark} % Current section name
     \fancyfoot[L]{Page\hspace{3pt}\thepage
     % \pgfmathprintnumber[fixed,fixed zerofill,precision=0,skip 0.,dec sep={}]{\thepage} % Aborted attempt to get leading-zero page num.
     }
     \fancyfoot[R]{\truncate{24em}{\@title}\hspace{2pt}\truncate{22em}{\footnotesize\textsc\@author}}
   }
   \fancypagestyle{plain} { % Section header pages
      \fancyhf{}
      \fancyhead[L]{\includegraphics[width=3cm]{\acLogoMain}}
      \fancyfoot[L]{Page\hspace{3pt}\thepage}
     \fancyfoot[R]{\truncate{24em}{\@title}\hspace{2pt}\truncate{22em}{\footnotesize\textsc\@author}}
   }
   % Suppress header/footer rules
   \renewcommand{\headrule}{}
   \renewcommand{\footrule}{}

   % For header/footer rules:
   % \renewcommand{\headrule}{
   %     \color{acorange}
   %     \hrule
   % }
   % \renewcommand{\footrule}{
   %     \color{acblue}
   %     \hrule
   % }
}

% Headers: change section name context in header for pages other than section title page.
\pagestyle{fancy}
\renewcommand{\chaptermark}[1]{\markboth{#1}{}}

% Footnotes: Prevent long ones folding onto next page.
\interfootnotelinepenalty=10000

% Tables: font, row & column spacing - moved to ac_table.py
% \setlength\tabcolsep{2 pt} %% Column-wise  cell spacing
% \renewcommand{\arraystretch}{1.1} %% Row-wise cell spacing
% \global\tabulinesep=^2pt_2pt % Top and bottom cell padding for longtabu


% Captions
\usepackage[font=normalsize,labelfont=bf,format=hang,labelformat=simple,justification=raggedright,singlelinecheck=false]{caption}
\addto\captionsenglish{\renewcommand{\figurename}{Figure }} %% Override Sphinx default "Fig."

% Set gaps between code-blocks and caption
% \setlength{\belowcaptionskip}{20pt}
% \setlength{\abovecaptionskip}{0pt}


% Contents page - bring body closer to title
\addtocontents{toc}{\vskip-40pt}

% Format the number in PDF bookmarks
\makeatletter
\renewcommand{\Hy@numberline}[1]{#1. }
\makeatother

% Can be used (within \begin{absolutelynopagebreak}, \end{absolutelynopagebreak} to prevent page breaks within, for e.g. tables.)
\newenvironment{absolutelynopagebreak}
  {\par\nobreak\vfil\penalty0\vfilneg
   \vtop\bgroup}
  {\par\xdef\tpd{\the\prevdepth}\egroup
   \prevdepth=\tpd}

\endinput
