# Usage: filter.py <command-name> <outputfile>
#
# Reads from stdin and only writes to stdout the lines
# that are not matched by the filter
# All lines read from stdin are written to the specified outfile

import sys
import re

if len(sys.argv) < 3:
    raise Exception("Usage: filter.py <command-name> <outputfile>")

# the name of the command, i.e. sphinx-build or pdflatex
command=sys.argv[1]
outfile=sys.argv[2]
print("Filtering output of %s, full log is appended to %s" % (command, outfile))

# patterns to *show* for latex output
latexPatterns = map(re.compile, [
    r"(?i)warning|error|fatal",
    r"^\! ",
    r"^l.[0-9]",
])

# patterns to *hide* for sphinx output
sphinxPatterns = map(re.compile, [
    r"^(reading sources|writing output|copying images)",
])

# Wrapper for filter function that returns False
# for N lines after the filter returns false
#number of lines to keep
def subsequentLinesFilter(N, filterfn):
    state = { 'postLineCount': 0 }
    def wrapper(line):
        filter=filterfn(line)
        if not filter:
            state['postLineCount'] = N
        elif state['postLineCount'] > 0:
            filter=False
            state['postLineCount'] -= 1
        return filter
    return wrapper

# By default all lines from latex are filtered out. Only
# lines with specific patterns are included.
# We probably want to refine this. Maybe we also want to print a number of
# subsequent lines or even preceiding lines in case of an error or warning,
# since latex tends to write info on multiple lines.

def filterLateX(line):
    filter=True
    if any(map(lambda pattern: re.search(pattern, line), latexPatterns)):
        filter=False
    return filter

def filterSphinx(line):
    filter=False
    if any(map(lambda pattern: re.search(pattern, line), sphinxPatterns)):
        filter=True
    return filter

def filterNothing(line):
    return False

skipcount=0

def newline():
    global skipcount
    sys.stdout.write("\n")
    skipcount = 0

def skip(line):
    global skipcount
    sys.stdout.write(".")
    skipcount += 1
    if skipcount > 100:
        newline()

def write(line):
    global skipcount
    if skipcount != 0:
        newline()
    sys.stdout.write(line)

with open(outfile, 'a') as logFile:
    logFile.write("** Output of %s ** \n" % command)
    
    filterfn=filterNothing
    if command == "pdflatex":
        filterfn=subsequentLinesFilter(1, filterLateX)
    if command == "sphinx-build":
        filterfn=filterSphinx

    for line in sys.stdin:
        logFile.write(line)
        if filterfn(line):
            skip(line)
        else:
            write(line)
    newline()


