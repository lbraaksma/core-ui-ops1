This glossary file should not be edited directly. Instead, edit the
_resources/glossary.tsv file and run bin/make_glossary.pl.