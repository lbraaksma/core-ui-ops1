.. CREATED BY make_glossary.pl - DO NOT EDIT!

.. glossary::
   :sorted:

   4-Eyes
   Four-Eyes
      A formal data cleansing and verification procedure whereby two distinct user roles whereby data changes are first inspected (by the :term:`Inspector`) then authorised (by the :term:`Guardian`). Modified suspect and exception data must pass a second review before the status is finalised.

   AC
      Asset Control (company).

   AC COM
      A plug-in for Microsoft Excel that allows interactive query of AC data.

   AC Datastore
   Datastore
      All stored Asset Control data, both static and time-series data, including data-models.

   AC Desktop
   AC Admin Desktop
      An MS Windows client for administering and interacting with AC data.

   ACDBA
      The AC Database Administrator. The name for the administrative account that has full permissions over the AC instance and auxiliary components. Also referred to as the system account name ``acdba``.

   ADO
      Asset Data Object. An organised collection of static and time-series attributes, with attached functions and meta-data. May represent any conceivable concept but commonly used to model financial entities (e.g. instruments, companies) and technical information (e.g. process parameters).

   ALE
      Analytics Library Extensions. An AC Server component that provides access to external libraries (e.g. MATLAB, R).

   API
      Application Programming Interface.

   Arbitration
   Arbitrated
      The process of determining which Normalized source attribute values propagate to the equivalent attribute in the associated Consolidated ADO.

   Attribute
      An Asset Control data-model element describing a value and type.

   BLOB
      Binary Large OBject. See `Wikipedia - Binary Large OBJect <https://en.wikipedia.org/wiki/Binary_large_object>`_.

   BMA
      Bid/Mid/Ask.

   Basic
   Raw
      Data as supplied by the vendor, with little or no conversion or transformation of coding systems and values.

   C0
   Consolidated
   Consolidation
      A Consolidated ADO is one whose attribute values have been based on one or more source Normalized ADOs. A Consolidated ADO uses the C0 (*c-zero*) data-model. See also :term:`Golden Copy`.

   CLOB
      `Character Large Object <http://docs.oracle.com/javadb/10.6.2.1/ref/rrefclob.html>`_

   CRC
      `Cyclic Redundancy Check <https://en.wikipedia.org/wiki/Cyclic_redundancy_check>`_

   DES
      `Data Encryption Standard <https://en.wikipedia.org/wiki/Data_Encryption_Standard>`_

   DLL
      `Dynamic-Linked Library <https://en.wikipedia.org/wiki/Dynamic-link_library>`_

   DTD
      `Document Type Definition <https://en.wikipedia.org/wiki/Document_type_definition>`_

   Data-feed
      Data provided by a data-vendor.

   Data-file
      The physical storage unit for Asset Control time-series data.

   Data-file Tree
      The hierarchy of data stored within a data-file.

   Data-model
      The basis of an ADO definition and the name for a collection of static and time-series attribute definitions, templates, lists and functions. A data-model is allocated a reserved prefix and must be loaded into AC Server before an instance of an ADO can be created. Data-models are normally stored as ``.tsv`` or ``ac_bl`` files.

   Derived
      A value produced by evaluating a formula.

   DML
      `Data Manipulation Language <https://en.wikipedia.org/wiki/Data_manipulation_language>`_

   ECMA
      `ECMA International <http://www.ecma-international.org>`_ is an industry association founded in 1961 and dedicated to the standardisation of Information and Communication Technology and Consumer Electronics.

   ECMAScript
      `ECMAScript <http://www.ecmascript.org>`_ is an object-oriented programming language for performing computations and manipulating computational objects within a host environment. It is standardised by :term:`ECMA`.

   Enriched
      Data-model elements containing values inferred from data-vendor provided data.

   Entity type
      (Consolidation) A classification of an ADO stored as a string value in ``C0_SRC25`` (for Consolidated ADOs) and ``N0_SRC25`` (for normalized ADOs).

   FCES
      Foreign Code Execution Server.

   FE
   Formula Engine
      Formula Engine. Asset Control's proprietary scripting language.

   Four-Eyes
      A formal data cleansing and verification procedure whereby two distinct user roles whereby data changes are first inspected (by the :term:`Inspector`) then authorised (by the :term:`Guardian`). Modified suspect and exception data must pass a second review before the status is finalised.

   FTP
      `File Transfer Protocol <https://en.wikipedia.org/wiki/File_Transfer_Protocol>`_

   Golden Copy
      Aggregation of multiple data sources into a single, standardised, universally maintained and distributed enterprise market data resource. Also occasionally referred to as Golden Source, Golden Data, Golden Record, Master Data.

   Guardian
      A user role in the four-eyes process with permission only to alter the status of data: to accept or reject changes made by an :term:`Inspector`.

   HTTP
      `Hypertext Transfer Protocol <https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol>`_

   IE
      AC Interface Engine.

   IECL
      AC Interface Engine Configurable Loader.

   IEDK
      AC Interface Engine Development Kit.

   IP
      `Internet Protocol address <https://en.wikipedia.org/wiki/IP_address>`_

   Inspector
      A user role in the :term:`four-eyes` process with permissions only to inspect and alter data. The inspector cannot not change the status of data items and therefore cannot approve or reject changes. This role be considered the first 'pair of eyes' in the concept of the four-eyes principle.

   Interface
      An Application Program that reads in data (typically from a vendor feed file) and populates a data model with it.

   JSON
      `JavaScript Object Notation <http://www.json.org>`_

   KDC
      Key Distribution Centre (for Kerberos).

   LDAP
      `Lightweight Directory Access Protocol <https://en.wikipedia.org/wiki/Lightweight_Directory_Access_Protocol>`_

   MTLM
      Master Transaction Log Marker - the position in the AC Server transaction log file of the most recent successful transaction.

   Match
   Matched
   Matching
      (Consolidation) The identification and grouping of normalized vendor ADOs with each other based on identity. :term:`Matching Rules` define what ADOs match and on what attribute value combinations.

   Matching Rules
      (Consolidation) Specifications of attribute value combinations indicating the same identity of an ADO. Normalized ADOs are matched with matching rules. The matched ADOs have various degrees of certainty of the validity of the match depending on which rule was used.

   NFS
      `Network File System <https://en.wikipedia.org/wiki/Network_File_System>`_

   N0
   Normalized
   Normalization
      Translation of vendor data to a common basis, allowing like-for-like comparison and matching. Normalized ADOs use the N0 (*n-zero*) data-model.

   Node
      Any hardware device, directly connected to the network, on which one or more Asset Control server processes (i.e, Query Server, Update Server, Replicator, or Notifier) is running.

   Notifier
      A process that publish messages for each transaction carried out on the :term:`AC Datastore`.

   OHLC
      `Open High Low Close <https://en.wikipedia.org/wiki/Open-high-low-close_chart>`_

   PAM
      `Pluggable Authentication Modules <https://en.wikipedia.org/wiki/Linux_PAM>`_

   PID
      Process Identifier

   POSIX
      `Portable Operating System Interface <https://en.wikipedia.org/wiki/POSIX>`_

   PTLM
      Private Transaction Log Marker. The position in the AC Server :term:`transaction log` of the latest send transaction to a specific destination.

   Query Server
      A process that enables front-end applications to retrieve data from the :term:`AC Datastore`.

   RDBMS
      Relational Database Management System.

   RIC
   RICs
      `Reuters Intrument Code <https://en.wikipedia.org/wiki/Reuters_Instrument_Code>`_

   RPC
      `Remote Procedure Call <https://en.wikipedia.org/wiki/Remote_procedure_call>`_

   Replication
      The process by which data and transactions occurring in one server installation are copied (in whole or in part) to one or more other installations. The :term:`Replication Server` performs this task.

   Replication Server
      A process that replicates data from one AC installation to other AC installations.

   SFTP
      `SSH File Transfer Protocol <https://en.wikipedia.org/wiki/SSH_File_Transfer_Protocol>`_

   SQL
      Structured Query Language.

   Static Attribute
      An attribute modelled to stored unchanging or infrequently changing data.

   Symbol
      The :term:`ADO` ID.

   TDBMS
      Time-series Database Management System.

   TSV
      Tab-Separated Values.

   Task
      (AC Workflow) A task is a step in a workflow that workflow items are passed through.

   Template
      A list or set of attributes, grouped to represent a concept or entity.

   Time-series
      A type of frequently changing data.

   Transaction Log
      A file-based store of transactions. Used to ensure transaction integrity, resilience and history.

   Transition
      (AC Workflow) A transition is a predefined condition that decides whether or not a workflow item can be moved to a subsequent task in the workflow.

   Translation Code
      A code given to a data-model element to uniquely identify it.

   Trigger
   Triggering
   Triggered
      A calculation caused by the update of an attribute value.

   Update Server
      A process that enables front-end applications to perform updates to the :term:`AC Datastore`.

   UTC
      `Coordinated Universal Time <https://en.wikipedia.org/wiki/Coordinated_Universal_Time>`_

   Validation
   Validate
      The process of reviewing data and marking it as 'good' data.

   Vendor
      A commercial provider of data.

   WFE
      AC Workflow Engine.

   Workflow item
   Workflow items
      A workflow item is an data object that must be passed through a workflow. AC Workflow Engine receives AC Server transactions and compares them to *begin* task *transition rules*. Transactions that enter the workflow create one or more *workflow items* associated with the transaction's ADO. (A workflow item represents the work to be done to an ADO as it passes through a workflow's tasks.) A workflow item passes from task to task according to the definition of each task's rules. Workflow items have their own *attributes* associated with them (e.g. priority, critical time and user/group assignment).

   Workflow
   Workflows
      A workflow is a series of tasks that are carried out in a specific order on workflow items.

