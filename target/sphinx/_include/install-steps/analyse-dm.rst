**Install data-model (Prior check)**

Analyse the data-model (compare the data-model in the database with the new
version in the ``.tsv`` files):

.. parsed-literal::

   $ cd $AC_SYSTEM/interfaces/|interface_name|
   $ bin/installdm

This creates an :file:`analyze.tsv` file in the current directory, containing
information about the new data-model, and whether any time-series stores must
be created.
