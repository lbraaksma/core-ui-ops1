**Update Consolidation C0**

.. code-block:: console

   $ cd $AC_SYSTEM/interfaces/CONSOLIDATION_C0
   $ bin/installdm -ut

.. hint::

   The ``-t`` option is new for Consolidation's ``installdm`` but subject to 
   change.
