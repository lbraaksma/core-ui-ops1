**Check Minimum Software Requirements are met**

Make sure your system component versions meet or exceed those specified in
:ref:`minimum-software-requirements`.
