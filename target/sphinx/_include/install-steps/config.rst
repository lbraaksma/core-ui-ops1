**Configure**

Before installing the data-model, the Interface Engine requires at least one of
the following to be configured:

* A global properties file:
  :file:`$AC_SYSTEM/interfaces/config/global.properties`. This may already
  exist if you have previously installed and run other AC interfaces.

* An interface-specific properties file. Values in this file override any set
  in the global file.

To create a :file:`global.properties` file based on the default template:

.. code-block:: console

   $ cd $AC_SYSTEM/interfaces/config
   $ cp global.properties-template global.properties

Edit the :file:`global.properties` file to set or check the following
properties:
   
- :samp:`ie4.acconnector.hostname={AC Server host name}`

- :samp:`ie4.acconnector.installationname={AC Server installation name}`

  This is the value of ``INSTALLATION`` as specified in the :file:`ac.ini` file.

- :samp:`ie4.acconnector.user={user name}`

  The account user name for connecting to AC Server (usually the ACDBA account
  name).

- :samp:`ie4.acconnector.passwordfile={path to password file}`

  The password file for the ACDBA user can be found in
  :samp:`$AC_SYSTEM/files/auto_logon/{user}`.
