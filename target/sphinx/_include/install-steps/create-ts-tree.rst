**Create the time-series data-file tree**

Run the following commands to create data-file trees for the supplied
examples:

.. parsed-literal::

   $ echo "TRANS
   OBJECT TREE ADD 
   symbol=\\"|interface_name|\\" 
   replicate=1 
   longname=\\"|interface_name|\\" 
   root=\\"|interface_name|\\"
   depth=10" | ac_bl -
     