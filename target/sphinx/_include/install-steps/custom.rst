**Customise**

Copy any customised data-model files to:

.. parsed-literal::

   $AC_SYSTEM/interfaces/|interface_name|/datamodel

