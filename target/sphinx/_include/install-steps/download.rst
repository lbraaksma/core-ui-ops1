**Download**

#. Log onto the |userweb|.
#. In the "|section|" section, follow the link for "|product| |release|".
#. Click the link in the *Application builds* section to download the package file.

The downloaded file name is: |downloaded_package|
