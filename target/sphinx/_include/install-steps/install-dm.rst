**Install data-model**

If no issues were found in the :file:`analyze.tsv` file, install the new data
model into the database:

.. code-block:: console

   $ bin/installdm -u
