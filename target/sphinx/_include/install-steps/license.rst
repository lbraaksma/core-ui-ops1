**License**

"|project|" is an independently licensed product.

Ensure your AC Server license key includes an entry for |project|.

In a terminal on the server running the AC Server instance:

.. parsed-literal::

   $ cd $AC_SYSTEM/files
   $ cat license.txt | grep -c |license_key|
   
This command should return the value ``1``. If it does not, contact your
AC Server system administrator or the `AC Helpdesk <helpdesk@asset-control.com>`_.