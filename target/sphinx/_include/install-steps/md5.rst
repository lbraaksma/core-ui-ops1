**File Integrity Check**

Check the integrity of the downloaded file:

.. parsed-literal::

   $ md5 |downloaded_package|


Compare the output text to the :guilabel:`MD5 checksum` stated in the
*Application builds* download link.
