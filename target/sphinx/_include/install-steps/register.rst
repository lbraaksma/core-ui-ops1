**Register**

Register the interface by running the :program:`menu.sh` program as follows:

.. code-block:: console

   $ cd $AC_SYSTEM/install
   $ ./menu.sh

#. Type :kbd:`3` and press :kbd:`Enter` for ``Asset Control interfaces``.
#. Enter the corresponding number for the |interface_name| interface and press :kbd:`Enter`. 
#. Return to the previous menu level by pressing :kbd:`Enter`.
#. Type :kbd:`6` (``Post installation tasks``) and press :kbd:`Enter`.
#. Type :kbd:`2` (``Register installed interfaces``) and press :kbd:`Enter`.
#. Exit the program by pressing :kbd:`Enter` twice.
