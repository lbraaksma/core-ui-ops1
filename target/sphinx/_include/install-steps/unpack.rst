**Unpack**

Unpack the package file under the ``$AC_SYSTEM`` directory.

For example, if you have copied the package file to ``$AC_WORKDIR``, use the
commands:

.. parsed-literal::

   $ cd $AC_SYSTEM
   $ gunzip $AC_WORKDIR/|package|.tar.gz
   $ tar xf $AC_WORKDIR/|package|.tar

.. hint::

   On Solaris, ``gtar`` should be used instead of ``tar``.
