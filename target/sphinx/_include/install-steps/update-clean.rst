**Clean up**

Manually delete the library files for the previous release.

.. parsed-literal::

   $ rm -rf $AC_SYSTEM/interfaces/|interface_prefix|/lib
