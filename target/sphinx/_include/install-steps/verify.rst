**Verify**

Verify the installation.

With AC Admin Desktop:

#. Select :menuselection:`Modules --> Administration --> Interface` and confirm
   the |project| interface is in the :guilabel:`Interface:` drop-down menu list.

#. Select :menuselection:`View --> Browser Tabs --> Static Attributes` and
   confirm the presence of the static attribute list.

With AC Desktop 3.8:

#. Select :menuselection:`Tools --> Template Editor` and confirm the presence
   of ``xxx ATTRIBUTES`` in the :guilabel:`Attribute tree`.
