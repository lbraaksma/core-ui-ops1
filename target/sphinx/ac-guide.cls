%
% ac-guide.cls for Sphinx (http://sphinx-doc.org/)
%
% A custom document class for PDF. Specifies:
% - Title page (title, subtitle, layout, graphics)
% - Table of contents format (level depth, alignment, styling)
% - Add warranty page (static or created by generatedocs.py)
% - End page (layout, graphics)
%
% Generic version (subtitle passed in via author tag)

\NeedsTeXFormat{LaTeX2e}[1995/12/01]
\ProvidesClass{ac-guide}[2009/06/02 Document class (Sphinx manual)]

% chapters starting at odd pages (overridden by 'openany' document option)
\PassOptionsToClass{openright}{\sphinxdocclass}

\newcommand{\acLogoMain}{MASTER-CMYK-2col.png}
\newcommand{\acLogoA}{MASTER_A-CMYK-1col_100pc_DOCUMENTATION_png.png}

% 'oneside' option overriding the 'twoside' default
\newif\if@oneside
\DeclareOption{oneside}{\@onesidetrue}
% Pass remaining document options to the parent class.
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{\sphinxdocclass}}
\ProcessOptions\relax

% Defaults two-side document
\if@oneside
% nothing to do (oneside is the default)
\else
\PassOptionsToClass{twoside}{\sphinxdocclass}
\fi

\usepackage{float}
\usepackage{courier}
\usepackage[pdftex]{graphicx}

% Full page background - cropped grey 'A'
\usepackage[
  pages=some,
  scale=1,
  angle=0,
  opacity=0.1
  ]{background}
\backgroundsetup{
contents={
\includegraphics[width=\paperwidth,height=\paperheight]{\acLogoA}}
}

\usepackage[abs]{overpic} % overpic, absolute positioning mode
\usepackage[export]{adjustbox} % For centering front/back page images
\usepackage{tikz} % For positioning of elements on page

% Avoids warning of too small headheight
\setlength{\headheight}{25pt}

\newcommand{\HRule}{\rule{\linewidth}{0.5mm}}

% Set some sane defaults for section numbering depth and TOC depth.  You can
% reset these counters in latex_preamble in conf.py
\setcounter{secnumdepth}{3}
\setcounter{tocdepth}{3}

\LoadClass{\sphinxdocclass}

% Layout of Title page
\renewcommand{\maketitle}{%
   \makeatletter
   \begin{titlepage}
      \BgThispage
      \fancyhf{}
      \begin{tikzpicture}[overlay, remember picture,
         shift={(current page.north west)}]
         % Horizontal rules
         \draw[gray,line width=0.5mm] (1cm,-8cm) -- (17.9cm,-8cm);
         \draw[gray,line width=0.5mm] (1cm,-24.5cm) -- (17.9cm,-24.5cm);
         % Main AC Logo
         \node[anchor=north west,
               xshift=1cm,
               yshift=-1cm]
              at (current page.north west) %left upper corner of the page
              {\includegraphics[width=5cm]{\acLogoMain}};
         % Document Title
         \node[anchor=north west,
               xshift=1cm,
               yshift=-8.6cm]
              at (current page.north west)
              {
              \begin{minipage}{16.7cm} % Wrap at end of Horizontal rules
              \raggedright % Avoid hyphenation
              \fontsize{40}{45}\selectfont
               \textbf{\@title}
               \\[12pt] % gap between title and subtitle
              \color{acorange}
                \fontsize{20}{22}\selectfont
                % \textbf{\textsc{\@author}} % Bold & Smallcaps
                \textsc{\@author} % Smallcaps only
              \end{minipage}
              };
         % Release number and date
         \node[anchor=north west,
               xshift=1cm,
               yshift=-25cm]
              at (current page.north west)
              {\(
              \arraycolsep=1.4pt\def\arraystretch{2.2}
              \begin{array}{lcl}
                  \textbf{Release} & \textbf{:} & \textsf{\acRelease} \\
                  \textbf{Date}    & \textbf{:} & \textsf{\today}
                  \end{array}
              \)};
      \end{tikzpicture}
    \end{titlepage}
    \makeatother
}
% End AC Title

% End Page - Empty end page with logo, copyright, URL
\newcommand{\acendpage}{%
   \BgThispage
   \clearpage
   % don't use courier font for URLs
   \urlstyle{same}
   % Add copyright as footer, remove rules
   \fancyhf{}
   \fancyfoot[C] {
   \copyright\hspace{2pt}\acCopyright
   }
   % Suppress header/footer rules
   \renewcommand{\headrule}{}
   \renewcommand{\footrule}{}
   \begin{center}
      \begin{tikzpicture}[overlay, remember picture,
         shift={(current page.north west)}]
         % Main AC Logo, same position as front page
         \node[anchor=north west,
               xshift=1cm,
               yshift=-1cm]
              at (current page.north west) %left upper corner of the page
              {\includegraphics[width=5cm]{\acLogoMain}};
      \end{tikzpicture}
   \end{center}
}
\AtEndDocument{\acendpage}
% End AC Final Page

% Catch the end of the {abstract} environment, but here make sure the abstract
% is followed by a blank page if the 'openright' option is used.
%
\let\py@OldEndAbstract=\endabstract
\renewcommand{\endabstract}{
  \if@openright
    \ifodd\value{page}
      \typeout{Adding blank page after the abstract.}
      \vfil\pagebreak
    \fi
  \fi
  \py@OldEndAbstract
}

% This wraps the \tableofcontents macro with all the magic to get the spacing
% right and have the right number of pages if the 'openright' option has been
% used.  This eliminates a fair amount of crud in the individual document files.
%
% Modified by AC to have a warranty and introduction
\let\py@OldTableofcontents=\tableofcontents
\renewcommand{\tableofcontents}{%
  \def\PYGZsq{\textquotesingle}%
  \acWarranty%
  \setcounter{page}{1}%
  \pagebreak%
  \pagestyle{plain}%
  {%
    \parskip = 0mm%
    \py@OldTableofcontents%
    \if@openright%
      \ifodd\value{page}%
        \typeout{Adding blank page after the table of contents.}%
        \pagebreak\hspace{0pt}%
      \fi%
    \fi%
    \cleardoublepage%
  }%
  \pagenumbering{arabic}%
  \@ifundefined{fancyhf}{}{\pagestyle{normal}}%
  \acIntroduction%
}
\pagenumbering{roman}

% Sets the width of section numbers in ToC
\renewcommand*\l@section{\@dottedtocline{1}{1.5em}{6em}}
\renewcommand*\l@subsection{\@dottedtocline{2}{3em}{6em}}

% Fix the bibliography environment to add an entry to the Table of
% Contents.
% For a report document class this environment is a chapter.
\let\py@OldThebibliography=\thebibliography
\renewcommand{\thebibliography}[1]{
  \cleardoublepage
  \phantomsection
  \py@OldThebibliography{1}
  \addcontentsline{toc}{chapter}{\bibname}
}

% Same for the indices.
% The memoir class already does this, so we don't duplicate it in that case.
%
\@ifclassloaded{memoir}{}{
  \let\py@OldTheindex=\theindex
  \renewcommand{\theindex}{
    \cleardoublepage
    \phantomsection
    \py@OldTheindex
    \addcontentsline{toc}{chapter}{\indexname}
  }
}
