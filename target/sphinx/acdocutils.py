#
import os
from datetime import datetime

def get_release_version(majorminorversion=None,buildtime=None):
    """Get the release version string."""
    # Get version variable set by Jenkins if available.
    # The environment variable IS_M2RELEASEBUILD should always be present 
    # in Jenkins builds.
    # The environment variable POM_VERSION should always be present in Jenkins 
    # builds (of type Maven) but will always contain the version that is 
    # in the pom at start-of-build. Usually this is the SNAPSHOT version, 
    # even when a release build is started so it is not usable in this 
    # context when we are releasing.
    # The environment variable MVN_RELEASE_VERSION contains the correct value
    # but this one is only available with a release build.
    # BTW, This trouble only exists because the design of the Maven release 
    # process is utter crap.
    # Non-Jenkins builds (no characteristic environment variables present) 
    # are assumed to be local builds.
    is_jenkins_release_build = os.getenv('IS_M2RELEASEBUILD', 'unset') == 'true'
    if is_jenkins_release_build:
        release = os.getenv('MVN_RELEASE_VERSION')
    elif 'POM_VERSION' in os.environ:
        release = os.getenv('POM_VERSION')
        if 'BUILD_NUMBER' in os.environ:
            release += "-build-%s" % os.getenv('BUILD_NUMBER')
    else:
        release = "%s.localbuild" % majorminorversion
        if buildtime:
            release += "-%s" % buildtime.strftime("%Y-%m-%dT%H%M%S")
            # release += "-%s" % buildtime.isoformat()
        else:
            release += "-%s" % datetime.now().strftime("%Y-%m-%dT%H%M%S")
            # release += "-%s" % datetime.now().replace(microsecond=0).isoformat()
    assert(release is not None)
    return release

if __name__ == "__main__":
    # Do some tests
    if 'IS_M2RELEASEBUILD' in os.environ:
        del os.environ['IS_M2RELEASEBUILD']
    if 'MVN_RELEASE_VERSION' in os.environ:
        del os.environ['MVN_RELEASE_VERSION']
    if 'POM_VERSION' in os.environ:
        del os.environ['POM_VERSION']
    dt = datetime(2010,10,18)
    assert(get_release_version("1.0",dt) == "1.0.localbuild-2010-10-18T00:00:00")
    assert(get_release_version(buildtime=dt) == "None.localbuild-2010-10-18T00:00:00")
    
    os.environ['POM_VERSION'] = "1.0.0-SNAPSHOT"
    assert(get_release_version("1.0") == "1.0.0-SNAPSHOT")
    assert(get_release_version() == "1.0.0-SNAPSHOT")

    os.environ['MVN_RELEASE_VERSION'] = "1.0.0"
    assert(get_release_version("1.0") == "1.0.0-SNAPSHOT")
    assert(get_release_version() == "1.0.0-SNAPSHOT")

    os.environ['BUILD_NUMBER'] = "42"
    assert(get_release_version("1.0") == "1.0.0-SNAPSHOT-build-42")
    assert(get_release_version() == "1.0.0-SNAPSHOT-build-42")
    
    os.environ['IS_M2RELEASEBUILD'] = "false"
    assert(get_release_version("1.0") == "1.0.0-SNAPSHOT-build-42")
    assert(get_release_version() == "1.0.0-SNAPSHOT-build-42")

    os.environ['IS_M2RELEASEBUILD'] = "true"
    assert(get_release_version("1.0") == "1.0.0")
    assert(get_release_version() == "1.0.0")

    del os.environ['MVN_RELEASE_VERSION']
    try:
        get_release_version("1.0")
    except AssertionError as expected:
        pass
