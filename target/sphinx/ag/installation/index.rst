.. _ag-installation:

############
Installation
############

A minimal fully core setup requires the presence of the following services:

* AC Server

The AC Core UI services are provided as docker images.

Setup
=====

Requirements
------------

* Docker
* AC Server 7
* docker image of AC Core UI
* docker image of AC UI Kit 
* docker image of AC Ops360 

It is assumed all listed requirements are present prior to installing the data lineage services. If not, please resolve this first.

AC Server
---------

Make sure that is enabled the AC Server.



Configuring the KIT
------------------------------------------

The settings for the services can be set using environment variables, which are passed to the docker container.


Environment variables for UI Kit 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- APPLICATION_SECRET:			   mandatory, 32 byte application secret
- ALLOWED_HOSTS:        		   mandatory, comma or whitespace separated string of hosts that are allowed to access the application
- PORT: 				   optional, http port the the service should run at, defaults to 9000
- HTTP_PORT:				   optional, same as PORT. If both are set, HTTP_PORT is used
- HTTPS_PORT:			           optional, the https port
- HTTPS_KEYSTORE_PATH:                     optional, path to the keystore for configuring SSL Certificates and keys
- HTTPS_KEY_STORE_TYPE:			   optional, path to the keystore type, defaults to JKS
- HTTPS_KEY_STORE_PASSWORD:		   only mandatory if ``HTTPS_KEYSTORE_PATH`` is set
- HTTPS_KEY_STORE_ALGORITHM:		   optional, the key store algorithm. Defaults to platforms default algorithm
- DB_CONTEXT_POOL_SIZE:                    optional, thread pool size for database operations. Defaults to 9.

Starting the Core UI Service
~~~~~~~~~~~~~~~~~~~~~~~~~

To start the UI service mapped to port 9009 on the host machine, you can use the ``docker run`` command. 
For example:

.. code-block:: console

   docker run --rm -it -p 9009:9000 -e APPLICATION_SECRET={SECRET_STRING} -e ALLOWED_HOSTS="0.0.0.0:9000" ac-core-UI-service:1.0.1
