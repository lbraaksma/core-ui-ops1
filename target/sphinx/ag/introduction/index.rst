
.. toctree::
   :maxdepth: 2

############
Introduction
############

******************
 What is |project|?
******************


|project| This is the core of the AC webservices, it consists of three parts:

* Core UI 
* UI Kit 
* Ops360 

With |project|, you create the foundation for the other webservices.

******************
How does it work?
******************

|project| runs as a collection of loosely coupled services in a microservice architecture. Each microservice (**Service**) implements a set of distinct building block.
