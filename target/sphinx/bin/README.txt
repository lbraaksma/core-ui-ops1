This script converts _resources/glossary.tsv entries into _include/glossary (an RST file that
can be included in any document).