#!/usr/bin/perl -w
# Write a rst glossary file from stdin tsv file containing:
# keyword[,keyword]<tab>Definition
# from sphinx directory:
# Usage: cat _resources/glossary.tsv | bin/make_glossary.pl > _include/glossary

use File::Basename;
my $prog = basename($0);

print ".. CREATED BY $prog - DO NOT EDIT!\n\n";

print ".. glossary::\n";
print "   :sorted:\n\n";

while (<STDIN>) {
   chomp;
   my @parts = split("\t");
   my @keys = split(",",$parts[0]);

   foreach my $kw (split (",",$parts[0])) {
     print "   $kw\n";
   }
   print "      $parts[1]\n";
   
   print "\n";

 }
