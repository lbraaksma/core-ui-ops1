# -*- coding: utf-8 -*-
import sys, os
from docutils.core import publish_file, publish_cmdline, publish_string
from datetime import datetime
from docutils.parsers.rst.directives.admonitions import BaseAdmonition
from sphinx.util import compat
compat.make_admonition = BaseAdmonition
import sphinx

sys.path.insert(0, os.path.abspath('.'))
sys.path.insert(0, os.path.abspath('./ext'))

import properties

# Useful functions for getting values from imported properties file
def getPropertyVal(propertyName, defaultValue):
    """gets a property from the included properties, returning the default value if the property does not exist."""
    result = defaultValue
    if hasattr(properties, propertyName):
        result = getattr(properties, propertyName)
    return result

def mergeDictProperty(propertyName, currentDict):
    """merges the dictionary from the included properties with the given dictionary, possibly overwriting existing values."""
    result = currentDict
    if hasattr(properties, propertyName):
        customDict = getattr(properties, propertyName)
        result = currentDict.copy()
        result.update(customDict)
    return result

def mergeListProperty(propertyName, currentList):
    """appends the list from from the included properties to the given list."""
    result = currentList
    if hasattr(properties, propertyName):
        customList = getattr(properties, propertyName)
        result = currentList + customList
    return result

# defaults for properties that can be overridden in your properties.py
project   = getPropertyVal('project', 'Product Documentation')
version   = getPropertyVal('version', '1.0')
release   = getPropertyVal('release', '1.0.0')
copyright = getPropertyVal('copyright', '%s, Asset Control International BV' % (datetime.now().year))
author    = getPropertyVal('author', 'docs@asset-control.com')

# set up rst replacements for properties
project_replacements = {} # Add any defaults here
mydict = globals().copy()
p_vals = mergeDictProperty('project_replacements', project_replacements)
mydict.update(p_vals)

rst_epilog = ""
for prop in [p for p in mydict if p[0] != '_']:
    rst_epilog += '\n.. |%s| replace:: %s' % (prop, mydict.get(prop))

# Define replacements for latex/html depending on tag passed from Makefile (or sphinx-build in case of generatedocs.py).
if tags.has('LATEX'):
    rst_epilog += '\n\n.. |finger| raw:: latex\n\n    \\texorpdfstring{\\includegraphics[height=1.0em,width=1.0em]{fa-hand-o-right.png}}{}\n'
    rst_epilog += '\n\n.. |rn-new| raw:: latex\n\n    \\texorpdfstring{\\includegraphics[height=1.0em,width=1.0em]{rn-new.png}}{}\n'
    rst_epilog += '\n\n.. |rn-fixed| raw:: latex\n\n    \\texorpdfstring{\\includegraphics[height=1.0em,width=1.0em]{rn-fixed.png}}{}\n'
    rst_epilog += '\n\n.. |rn-updated| raw:: latex\n\n    \\texorpdfstring{\\includegraphics[height=1.0em,width=1.0em]{rn-updated.png}}{}\n'
    rst_epilog += '\n\n.. |tick| raw:: latex\n\n    \\texorpdfstring{\\includegraphics[height=1.0em,width=1.0em]{black-tick.png}}{}\n'
    rst_epilog += '\n\n.. |green-tick| raw:: latex\n\n    \\texorpdfstring{\\includegraphics[height=1.0em,width=1.0em]{green-tick.png}}{}\n'
    rst_epilog += '\n\n.. |red-cross| raw:: latex\n\n    \\texorpdfstring{\\includegraphics[height=1.0em,width=1.0em]{red-cross.png}}{}\n'
    rst_epilog += '\n\n.. |long-arrow-right| raw:: latex\n\n    \\texorpdfstring{\\includegraphics[height=1.0em,width=1.0em]{long-arrow-right.png}}{}\n'
    rst_epilog += '\n\n.. |long-arrow-down| raw:: latex\n\n    \\texorpdfstring{\\includegraphics[height=1.0em,width=1.0em]{long-arrow-down.png}}{}\n'
    rst_epilog += '\n\n.. |book| raw:: latex\n\n    \\texorpdfstring{\\includegraphics[height=1.0em,width=1.0em]{fa-book.png}}{}\n'
    rst_epilog += '\n\n.. |begin-landscape| raw:: latex\n\n    \\begin{landscape}\n'
    rst_epilog += '\n\n.. |end-landscape| raw:: latex\n\n    \\end{landscape}\n'
    rst_epilog += '\n\n.. |pagebreak| raw:: latex\n\n    \\pagebreak\n'
    rst_epilog += '\n\n.. |userweb| raw:: latex\n\n    \\href{http://services.asset-control.com/userweb/}{Asset Control Userweb}\n' # Current location of userweb
    rst_epilog += '\n\n.. |portal| raw:: latex\n\n    \\href{https://portal.asset-control.com/}{Asset Control Customer Portal}\n' # Current location of portal
    rst_epilog += '\n\n.. |help-content-start| replace:: %% help-content-start\n\n'
    rst_epilog += '\n\n.. |help-content-end| replace:: %% help-content-end\n\n'
else:
    rst_epilog += '\n\n.. |finger| image:: /gfx/fa-hand-o-right.png\n    :height: 1.0em\n    :width: 1.0em\n'
    rst_epilog += '\n\n.. |rn-new| image:: /gfx/rn-new.png\n    :height: 1.0em\n    :width: 1.0em\n'
    rst_epilog += '\n\n.. |rn-fixed| image:: /gfx/rn-fixed.png\n    :height: 1.0em\n    :width: 1.0em\n'
    rst_epilog += '\n\n.. |rn-updated| image:: /gfx/rn-updated.png\n    :height: 1.0em\n    :width: 1.0em\n'
    rst_epilog += '\n\n.. |tick| image:: /gfx/black-tick.png\n    :height: 1.0em\n    :width: 1.0em\n'
    rst_epilog += '\n\n.. |green-tick| image:: /gfx/green-tick.png\n    :height: 1.0em\n    :width: 1.0em\n'
    rst_epilog += '\n\n.. |red-cross| image:: /gfx/red-cross.png\n    :height: 1.0em\n    :width: 1.0em\n'
    rst_epilog += '\n\n.. |long-arrow-right| image:: /gfx/long-arrow-right.png\n    :height: 1.0em\n    :width: 1.0em\n'
    rst_epilog += '\n\n.. |long-arrow-down| image:: /gfx/long-arrow-down.png\n    :height: 1.0em\n    :width: 1.0em\n'
    rst_epilog += '\n\n.. |book| image:: /gfx/fa-book.png\n    :height: 1.0em\n    :width: 1.0em\n'
    rst_epilog += '\n\n.. |begin-landscape| unicode:: U+0020\n'
    rst_epilog += '\n\n.. |end-landscape| unicode:: U+0020\n'
    rst_epilog += '\n\n.. |pagebreak| unicode:: U+0020\n'
    rst_epilog += '\n\n.. |userweb| replace:: `Asset Control Userweb <http://services.asset-control.com/userweb/>`__\n' # Current location of userweb
    rst_epilog += '\n\n.. |portal| replace:: `Asset Control Customer Portal <https://portal.asset-control.com/>`__\n' # Current location of portal
    rst_epilog += '\n\n.. |help-content-start| raw:: html\n\n    <!-- content_start -->\n\n'
    rst_epilog += '\n\n.. |help-content-end| raw:: html\n\n    <!-- content_end -->\n\n'

project_basename = properties.project.replace(" ", "").replace("\'", "").replace("\"", "")

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#sys.path.insert(0, os.path.abspath('.'))

# -- General configuration ------------------------------------------------

# If your documentation needs a minimal Sphinx version, state it here.
needs_sphinx = '1.5.3'

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.

# IMPORTANT: Only put official sphinx extensions here. Custom AC extensions
# should be added in your properties.py file.
# This is because sphinx upgrades can cause extensions to break. Placing
# customs ones outside here allows you to test with and without custom
# extensions by commenting out each extension in your local properties.py.

extensions = mergeListProperty('extensions', [ 'numfig'
                                             , 'figtable'
                                             , 'highlighting'
                                             , 'sphinx.ext.todo'
                                             , 'sphinx.ext.graphviz'
                                             , 'sphinxcontrib.blockdiag'
                                             , 'sphinx.ext.intersphinx'
                                             , 'sphinx.ext.imgmath'
                                             # Desired but not installed on build servers:
                                             # , 'sphinxcontrib.bibtex'
                                             # , 'sphinxcontrib.programoutput'
                                         ])

# Test of links between sphinx projects
intersphinx_mapping = {
    'acserver': ('http://techdoc.asset-control.com/docs/Applications/AC Server 7.3', None)
}


# Turns on numbered figures for HTML output
number_figures = getPropertyVal('number_figures', True)

numfig_figure_caption_prefix = "Figure"

# To include .. todo:: notes in the target document, set the following in properties.py
todo_include_todos = getPropertyVal('todo_include_todos', False)

# Add any paths that contain templates here, relative to this directory.
templates_path = getPropertyVal('templates_path', ['_templates'])

# The suffix of source filenames.
source_suffix = '.rst'

# The encoding of source files.
source_encoding = 'utf-8-sig'

# The master toctree document.
master_doc = getPropertyVal('master_doc', 'index')

# The language for content autogenerated by Sphinx. Refer to documentation
# for a list of supported languages.
#language = None

# There are two options for replacing |today|: either, you set today to some
# non-false value, then it is used:
#today = ''
# Else, today_fmt is used as the format for a strftime call.
#today_fmt = '%B %d, %Y'

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
exclude_patterns = mergeListProperty('exclude_patterns', ['_build',
                                                          '_include',
                                                          '_resources',
                                                          '_static',
                                                          '_templates',
                                                          '_themes',
                                                          'logs',
                                                          '**/_archive' # _archive can be anywhere
                                                      ])

# The reST default role (used for this markup: `text`) to use for all
# documents.
#default_role = None

# If true, '()' will be appended to :func: etc. cross-reference text.
#add_function_parentheses = True

# If true, the current module name will be prepended to all description
# unit titles (such as .. function::).
#add_module_names = True

# If true, sectionauthor and moduleauthor directives will be shown in the
# output. They are ignored by default.
# show_authors = True

# The name of the Pygments (syntax highlighting) style to use.
pygments_style = getPropertyVal('pygments_style', 'sphinx')

# A list of ignored prefixes for module index sorting.
#modindex_common_prefix = []

# If true, keep warnings as "system message" paragraphs in the built documents.
#keep_warnings = False

# -- Options for HTML output ----------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.

#html_theme = "basic"
#html_theme_options = {}
#html_theme_options = mergeDictProperty('html_theme_options', html_theme_options)

# ---- 'Read The Docs' theme - Copied to _themes
# html_theme = "sphinx_rtd_theme"
# html_theme_path = ['_themes']
# html_theme_options = getPropertyVal('html_theme_options',{'display_version': True})


# The name for this set of Sphinx documents.  If None, it defaultProps to
# "<project> v<release> documentation".
html_title = getPropertyVal('html_title', properties.project)

# A shorter title for the navigation bar.  Default is the properties project.
# html_short_title = getPropertyVal('html_short_title', properties.project)

# The name of an image file (relative to this directory) to place at the top
# of the sidebar.
html_logo = getPropertyVal('html_logo', 'gfx/MASTER CMYK 1col-WO.png')

# The name of an image file (within the static path) to use as favicon of the
# docs.  This file should be a Windows icon file (.ico) being 16x16 or 32x32
# pixels large.
html_favicon = getPropertyVal('html_favicon', 'gfx/MASTER RGB 1col_WO.ico')

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = mergeListProperty('html_static_path', ['_static'])

# Add any extra paths that contain custom files (such as robots.txt or
# .htaccess) here, relative to this directory. These files are copied
# directly to the root of the documentation.
#html_extra_path = []

# If not '', a 'Last updated on:' timestamp is inserted at every page bottom,
# using the given strftime format.
html_last_updated_fmt = '%c'

# If true, SmartyPants will be used to convert quotes and dashes to
# typographically correct entities.
#html_use_smartypants = True

# Custom sidebar templates, maps document names to template names.
# html_sidebars = getPropertyVal('html_sidebars', {'**': ['relations.html','globaltoc.html','localtoc.html'],})

# Additional templates that should be rendered to pages, maps page names to
# template names.
#html_additional_pages = {}

# If false, no module index is generated.
#html_domain_indices = True

# If false, no index is generated.
html_use_index = getPropertyVal('html_use_index', True)

# If true, the index is split into individual pages for each letter.
#html_split_index = False

# If true, links to the reST sources are added to the pages.
html_show_sourcelink = False

# If true, "Created using Sphinx" is shown in the HTML footer. Default is True.
html_show_sphinx = getPropertyVal('html_show_sphinx', False)

# If true, "(C) Copyright ..." is shown in the HTML footer. Default is True.
html_show_copyright = getPropertyVal('html_show_copyright', True)

# If true, an OpenSearch description file will be output, and all pages will
# contain a <link> tag referring to it.  The value of this option must be the
# base URL from which the finished HTML is served.
#html_use_opensearch = ''

# This is the file name suffix for HTML files (e.g. ".xhtml").
#html_file_suffix = None

# Output file base name for HTML help builder.
htmlhelp_basename = getPropertyVal('htmlhelp_basename', project_basename)

# -- Options for LaTeX output ---------------------------------------------

import codecs
import os.path

def rstToTexSnippet(rstDoc):
    rstFileName = rstDoc # + ".rst"
    if os.path.isfile(rstFileName):
        f = codecs.open(rstFileName, encoding='utf-8')
        rstSnippet = f.read()
        f.close()
        settings = {"doctitle_xform":False, "input_encoding":"unicode", "output_encoding":"unicode"}
        texSnippet = publish_string(rstSnippet, writer_name="latex", settings_overrides=settings)
        texSnippet = texSnippet.split(r"\begin{document}")[1]
        texSnippet = texSnippet.split(r"\end{document}")[0]
        texSnippet = texSnippet.replace(r"\section", r"\section*")
        texSnippet = texSnippet.replace(r"\subsection", r"\subsection*")
    else:
        texSnippet = ""
    return texSnippet

warranty = rstToTexSnippet('warranty')
introduction = rstToTexSnippet('introduction')
chapter_text = getPropertyVal('chapter_text', "")
# Which level to show titles in ToC (PDF only)
# 0 = Only level 1 titles
# 1 = Levels 1 & 2
# 2 = Levels 1, 2 & 3 etc.
tocdepth  = getPropertyVal('tocdepth', '2')

secnumdepth = getPropertyVal('secnumdepth','9')

style = 'ac-guide'
stylefile = style + '.sty'

default_preamble = \
u"""
\\newcommand {\\acRelease}{%s}
\\newcommand {\\acWarranty}{%s}
\\newcommand {\\acIntroduction}{%s}
\\newcommand {\\acCopyright}{%s}
\\usepackage{%s}
%% Section prefix text
\\makeatletter
%% Set "chapter" text - Changeable to 'Section' (or whatever) in properties.py
\\renewcommand{\\@chapapp}{%s}
\\makeatother
\\setcounter{tocdepth}{%s}
\\setcounter{secnumdepth}{%s}
%% Set table header row colour
%% _templates files (new) or ac_table.py (old) use this option.
\\protected\\def\\sphinxstyletheadfamily{\\cellcolor{header}\\sffamily}
%% Sphinx 1.6.5+ custom style tabularcolumns specs.
%% See https://github.com/sphinx-doc/sphinx/issues/4196#issuecomment-340720569
\\newcolumntype{\\Yl}[1]{>{\\raggedright\\arraybackslash}\\Y{#1}}
\\newcolumntype{\\Yr}[1]{>{\\raggedleft\\arraybackslash}\\Y{#1}}
\\newcolumntype{\\Yc}[1]{>{\\centering\\arraybackslash}\\Y{#1}}
""" % (properties.release,
       warranty,
       introduction,
       copyright,
       style, # selectable ac-guide to cope with different sphinx versions
       chapter_text,
       tocdepth,
       secnumdepth)

preamble = getPropertyVal('preamble', default_preamble)

default_latex_elements = {
    # The paper size ('letterpaper' or 'a4paper').
    'papersize': 'a4paper',
    # The font package
    'fontpkg': '\\usepackage[scaled=.875]{helvet}\\renewcommand*\\familydefault{\\sfdefault}',
    'pointsize': '10pt', # The font size ('10pt', '11pt' or '12pt').
    'classoptions': ',openany,oneside',
    'babel': '\\usepackage[english]{babel}',
    # Additional stuff for the LaTeX preamble.
    'preamble': preamble,
    'fncychap': '\\usepackage[Sonny]{fncychap}',
    'figure_align': 'H'
}

latex_elements = mergeDictProperty('latex_elements', default_latex_elements)

latex_additional_files = mergeListProperty('latex_additional_files',
    [
    'gfx/fa-hand-o-right.png',
    'gfx/rn-new.png',
    'gfx/rn-fixed.png',
    'gfx/rn-updated.png',
    'gfx/black-tick.png',
    'gfx/green-tick.png',
    'gfx/red-cross.png',
    'gfx/long-arrow-right.png',
    'gfx/long-arrow-left.png',
    'gfx/long-arrow-up.png',
    'gfx/long-arrow-down.png',
    'gfx/fa-book.png',
    'img/fa-user-r.png',
    'img/fa-user-g.png',
    'img/fa-user-b.png',
    'ac-guide.cls',
    stylefile,
    'tabu.sty',
    # new logos
    'gfx/MASTER-CMYK-2col.png',
    'gfx/MASTER-RGB-1col.png',
    'gfx/MASTER-RGB-1col_WO.png',
    'gfx/MASTER_A-CMYK-1col_100pc_DOCUMENTATION_png.png'
    ])

default_latex_title = u'%s \\texttrademark{} %s' % (project.replace("_", "\\_"), version)

latex_title = getPropertyVal('latex_title', default_latex_title)

# Grouping the document tree into LaTeX files. List of tuples
# (source start file, target name, title,
#  author, documentclass [howto, manual, or own class]).
default_latex_documents = [ ('index', '%s.tex' % (project_basename), latex_title, author, 'ac-guide') ]

latex_documents = getPropertyVal('latex_documents', default_latex_documents)

# The name of an image file (relative to this directory) to place at the top of
# the title page.
latex_logo = None

# For "manual" documents, if this is true, then toplevel headings are parts,
# not chapters.
latex_use_parts = False

# If true, show page references after internal links.
#latex_show_pagerefs = False

# If true, show URL addresses after external links.
#latex_show_urls = False

# Documents to append as an appendix to all manuals.
#latex_appendices = []

# If false, no module index is generated.
#latex_domain_indices = True

# -- Options for manual page output ---------------------------------------

# One entry per manual page. List of tuples
# (source start file, name, description, authors, manual section).
default_man_pages = [
    ('index', project_basename.lower(), u'%s %s Documentation' % (project, version),
     [author], 1)
]

man_pages = getPropertyVal('man_pages', default_man_pages)

# If true, show URL addresses after external links.
#man_show_urls = False

# For blockdiag
blockdiag_fontpath = []
blockdiag_html_image_format = 'PNG'
blockdiag_latex_image_format = 'PNG'

# For graphviz
graphviz_dot_args = ['-v']
graphviz_output_format = 'svg' # 'png' is default
os.environ["SERVER_NAME"] = "techdoc.asset-control.com"
os.environ["GV_FILE_PATH"] = "_images"
