#########
Downloads
#########

**PDF (A4 format)**

* :download:`ac-core-ui-ops-doc-AG.pdf<_build/latex/ac-core-ui-ops-doc-AG.pdf>` (104.66 KB)

* :download:`ac-core-ui-ops-doc-RN.pdf<_build/latex/ac-core-ui-ops-doc-RN.pdf>` (147.03 KB)


**HTML (Plain)**


**HTML (Themed)**



.. tip::

   * HTML is provided in themed and plain versions.
   * Download and unpack your choice of archive according to your platform:

     - ``tgz`` is a tar/gzip archive for UNIX and Linux.
     - ``zip`` for MS Windows.

   * In the expanded folder, open the ``index.html`` file in a web browser.

