from sphinx.writers.latex import LaTeXTranslator

# Chapters after the directive .. ac-appendix:: will be rendered as real
# (latex) appendices in the pdf. In HTML they are still normal chapters
# currently.
def setup(app):
    app.add_node(appendix,
                 html=(visit_appendix_node, depart_appendix_node),
                 latex=(latex_visit_appendix_node, latex_depart_appendix_node),
                 text=(visit_appendix_node, depart_appendix_node))

    app.add_directive('ac-appendix', ModeDirective)
    app.connect('doctree-resolved', process_appendix_nodes)
    app.connect('env-purge-doc', purge_appendixs)

from docutils import nodes

class appendix(nodes.General, nodes.Element):
    pass

def visit_appendix_node(self, node):
    return

def depart_appendix_node(self, node):
    return

def latex_visit_appendix_node(self, node):
    self.body.append("\\appendix")

def latex_depart_appendix_node(self, node):
    return

from docutils.parsers.rst.directives.admonitions import BaseAdmonition as make_admonition
from docutils.parsers.rst import Directive
from sphinx.locale import _

class ModeDirective(Directive):
    def run(self):
    	return [appendix('')]

def purge_appendixs(app, env, docname):
    return

def process_appendix_nodes(app, doctree, fromdocname):
    return
