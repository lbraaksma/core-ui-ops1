from sphinx.writers.latex import LaTeXTranslator
import sphinx.writers.latex
from docutils import nodes

# Adjusted Latex writer to produce \autoref for references

sphinx.writers.latex.HEADER += '\n\usepackage{hyperref}'

def setup(sphinx):
    LaTeXTranslator.visit_target = visit_target
    LaTeXTranslator.visit_reference = visit_reference
    
def visit_target(self, node):
    def add_target(id):
        # indexing uses standard LaTeX index markup, so the targets
        # will be generated differently
        if id.startswith('index-'):
            return
        # do not generate \phantomsection in \section{}
        anchor = not self.in_title
        self.body.append(self.hypertarget(id, anchor=anchor))

    # postpone the labels until after the sectioning command
    parindex = node.parent.index(node)
    try:
        try:
            next = node.parent[parindex+1]
        except IndexError:
            # last node in parent, look at next after parent
            # (for section of equal level) if it exists
            if node.parent.parent is not None:
                next = node.parent.parent[
                    node.parent.parent.index(node.parent)]
            else:
                raise
        if isinstance(next, nodes.section):
            if node.get('refid'):
                self.next_section_ids.add(node['refid'])
            self.next_section_ids.update(node['ids'])
            return
        elif isinstance(next, nodes.figure):
            # labels for figures go in the figure body, not before
            if node.get('refid'):
                self.next_figure_ids.add(node['refid'])
            self.next_figure_ids.update(node['ids'])
            return
        elif isinstance(next, nodes.table):
            # same for tables, but only if they have a caption
            # Adjusted by Asset Control
            # Fixed a bug here in the original latex writer: we should iterate over next, not over the current node
            for n in next:
                if isinstance(n, nodes.title):
                    if node.get('refid'):
                        self.next_table_ids.add(node['refid'])
                    self.next_table_ids.update(node['ids'])
                    return
            return
    except IndexError:
        pass
    if 'refuri' in node:
        return
    if node.get('refid'):
        add_target(node['refid'])
    for id in node['ids']:
        add_target(id)
    
def visit_reference(self, node):
    uri = node.get('refuri', '')
    if not uri and node.get('refid'):
        uri = '%' + self.curfilestack[-1] + '#' + node['refid']
    if self.in_title or not uri:
        self.context.append('')
    elif uri.startswith('mailto:') or uri.startswith('http:') or \
             uri.startswith('https:') or uri.startswith('ftp:'):
        self.body.append('\\href{%s}{' % self.encode_uri(uri))
        # if configured, put the URL after the link
        show_urls = self.builder.config.latex_show_urls
        if node.astext() != uri and show_urls and show_urls != 'no':
            if uri.startswith('mailto:'):
                uri = uri[7:]
            if show_urls == 'footnote' and not \
               (self.in_footnote or self.in_caption):
                # obviously, footnotes in footnotes are not going to work
                self.context.append(
                    r'}\footnote{%s}' % self.encode_uri(uri))
            else:  # all other true values (b/w compat)
                self.context.append('} (%s)' % self.encode_uri(uri))
        else:
            self.context.append('}')
    elif uri.startswith('#'):
        # references to labels in the same document
        id = self.curfilestack[-1] + ':' + uri[1:]
        self.body.append(self.hyperlink(id))
        if self.builder.config.latex_show_pagerefs and not \
                self.in_production_list:
            self.context.append('}} (%s)' % self.hyperpageref(id))
        else:
            self.context.append('}}')
    elif uri.startswith('%'):
        # references to documents or labels inside documents
        hashindex = uri.find('#')
        if hashindex == -1:
            # reference to the document
            id = uri[1:] + '::doc'
        else:
            # reference to a label
            id = uri[1:].replace('#', ':')
        # Adjusted by Asset Control    
        # Detect whether we are a reference to a table, section or figure
        # We do this by checking whether we have an inline node as child
        # In this case we use Latex's autoref instead of a hyperref
        # In all other cases we still use hyperref because else we effect the working of the envvar role and similar roles
        is_simple_ref = node.children is not None and len(node.children) == 1 and isinstance(node.children[0], nodes.inline)
        if is_simple_ref:
            self.body.append('{\\autoref{%s' % self.idescape(id))
            # We need to remove the child emphasis node for a simple reference
            del node.children[:]
        else:
            self.body.append(self.hyperlink(id))
        if len(node) and hasattr(node[0], 'attributes') and \
               'std-term' in node[0].get('classes', []):
            # don't add a pageref for glossary terms
            self.context.append('}}')
        else:
            if self.builder.config.latex_show_pagerefs:
                self.context.append('}} (%s)' % self.hyperpageref(id))
            else:
                self.context.append('}}')
    else:
        self.builder.warn('unusable reference target found: %s' % uri,
                          (self.curfilestack[-1], node.line))
        self.context.append('')