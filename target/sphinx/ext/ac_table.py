# -*- coding: utf-8 -*-
# Override the latex table writer to use longtabu
# NOTE: This file must be phased out as every sphinx update causes it
# to fail. Work in progress to implement functionality using standard docutils
# and/or sphinx configuration
#
from sphinx.writers.latex import LaTeXTranslator
from sphinx.locale import _
import sphinx

# Adjusted Latex writer to produce longtabu for tables and use labels

def setup(sphinx):
    LaTeXTranslator.depart_table =  depart_table

# Most of the following comes from <python2.7>/dist-packages/sphinx/writers/latex.py
def depart_table(self, node):
    self.table.longtable = True
    self.popbody()

    endmacro = '\n\\end{longtabu}\n'
    set_size(self,node)
    self.body.append('\n\\begin{longtabu}')

    if self.table.colspec:
        self.body.append(self.table.colspec)
    else:
        if self.table.has_problematic:
            colwidth = 0.95 / self.table.colcount
            colspec = ('p{%.3f\\linewidth}|' % colwidth) * \
                self.table.colcount
            self.body.append('{|' + colspec + '}\n')
        elif self.table.longtable:
            self.body.append('{|' + ('l|' * self.table.colcount) + '}\n')
        else:
            self.body.append('{|' + ('L|' * self.table.colcount) + '}\n')
    if self.table.caption is not None:
        self.body.append(u'\\caption{')
        for caption in self.table.caption:
            self.body.append(caption)
        self.body.append('}')

        # Where code breaks with sphinx upgrade
        # if sphinx.__version__ > '1.3.3':
        for id in self.pop_hyperlink_ids('table'):
            self.body.append(self.hypertarget(id, anchor=False))
        # else: # 1.3.3
        #     for id in self.next_table_ids:
        #         self.body.append(self.hypertarget(id, anchor=False))

        self.body.append('\\\\\n')

    set_header_rows(self,node)

    self.body.append('\\hline\n')
    self.body.extend(self.tableheaders)
    self.body.append('\n\\endfirsthead\n\n')

    reset_header_rows(self,node)

    self.body.append('\\multicolumn{%s}{c}\n' % self.table.colcount)
    self.body.append(r'{{\textsf{\tablename\ \thetable{} -- %s}}} \\' % ('continued from previous page'))
    self.body.append('\n\\hline\n')

    set_header_rows(self,node)

    self.body.extend(self.tableheaders)
    
    # Thin/think header rule
    set_rule(self,node)
    
    self.body.append('\n\\endhead\n')
    
    reset_header_rows(self,node)
        
    self.body.append(r'\hline \multicolumn{%s}{|r|}{{\textsf{%s}}} \\ \hline' % (self.table.colcount, ('Continued on next page')))
    self.body.append('\\endfoot\n')
    self.body.append('\\endlastfoot\n')
    
    # Thin/think header rule
    set_rule(self,node)

    set_data_rows(self,node)

    self.body.extend(self.tablebody)
    self.body.append(endmacro)
    # if sphinx.__version__ > '1.3.3':
    self.unrestrict_footnote(node)
    self.table = None
    self.tablebody = None

# Header row shading
def set_header_rows(self, node):
    if 'hdplain' in node['classes']:
        self.body.append('\\taburowcolors{white..white}\n')
    else: # default
        self.body.append('\\taburowcolors{header..header}\n')
        # self.body.append('\\rowfont{\\bfseries}\n')

def reset_header_rows(self, node):
    self.body.append('\\taburowcolors{white..white}\n')

# Data row shading
def set_data_rows(self, node):
    if 'rwshade' in node['classes']:
        self.body.append('\\taburowcolors{evenrow..oddrow}\n')
    else: # normal default
        self.body.append('\\taburowcolors{white..white}\n')

def set_rule(self,node):
    if 'rule' in node['classes']:
        self.body.append('\n\\hline\\hline\\hline\n') # Thicker header rule

def set_size(self,node):
    if 'small' in node['classes'] or 'data' in node['classes']:
        self.body.append('\\AtBeginEnvironment{longtabu}{\\scriptsize}\n') # Size for cell text
        self.body.append('\\AtBeginEnvironment{longtabu}{\\renewcommand{\\code}[1]{{\\scriptsize\\texttt{#1}}}}') # Size for code text
        
    else: # normal default
        self.body.append('\\AtBeginEnvironment{longtabu}{\\normalsize}\n') 
        self.body.append('\\AtBeginEnvironment{longtabu}{\\renewcommand{\\code}[1]{{\\normalsize\\texttt{#1}}}}')
