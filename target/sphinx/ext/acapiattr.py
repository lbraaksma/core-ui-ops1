
from sphinx.roles import XRefRole
from sphinx import addnodes
from sphinx.locale import _, l_
from docutils import nodes
from sphinx.domains.std import EnvVarXRefRole, GenericObject

class acapiattr(nodes.General, nodes.Element):
    pass

def visit_acapiattr_node(self, node):
    pass

def depart_acapiattr_node(self, node):
    pass


class AcApiAttrRole(XRefRole):
    
    def result_nodes(self, document, env, node, is_ref):
        if not is_ref:
            return [node], []
        varname = node['reftarget']
        tgtid = 'index-%s' % env.new_serialno('index')
        indexnode = addnodes.index()
        indexnode['entries'] = [
            ('single', varname, tgtid, ''),
            ('single', _('ac api attribute; %s') % varname, tgtid, '')
        ]
        targetnode = nodes.target('', '', ids=[tgtid])
        document.note_explicit_target(targetnode)
        return [indexnode, targetnode, node], []

class AcApiAttrDirective(GenericObject):
    indextemplate = l_('ac api attribute; %s')

def setup(app):
    app.add_directive('acapiattr', AcApiAttrDirective)
    app.add_role('acapiattr', AcApiAttrRole())
    app.add_node(acapiattr,
        html=(visit_acapiattr_node, depart_acapiattr_node),
        latex=(visit_acapiattr_node, depart_acapiattr_node),
        text=(visit_acapiattr_node, depart_acapiattr_node))
    app.add_object_type('acapiattr', 'acapiattr')


