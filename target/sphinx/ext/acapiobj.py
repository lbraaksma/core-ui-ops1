
from sphinx.roles import XRefRole
from sphinx import addnodes
from sphinx.locale import _, l_
from docutils import nodes
from sphinx.domains.std import EnvVarXRefRole, GenericObject

class acapiobj(nodes.General, nodes.Element):
    pass

def visit_acapiobj_node(self, node):
    pass

def depart_acapiobj_node(self, node):
    pass


class AcApiObjRole(XRefRole):
    
    def result_nodes(self, document, env, node, is_ref):
        if not is_ref:
            return [node], []
        varname = node['reftarget']
        tgtid = 'index-%s' % env.new_serialno('index')
        indexnode = addnodes.index()
        indexnode['entries'] = [
            ('single', varname, tgtid, ''),
            ('single', _('ac api object; %s') % varname, tgtid, '')
        ]
        targetnode = nodes.target('', '', ids=[tgtid])
        document.note_explicit_target(targetnode)
        return [indexnode, targetnode, node], []

class AcApiObjDirective(GenericObject):
    indextemplate = l_('ac api object; %s')

def setup(app):
    app.add_directive('acapiobj', AcApiObjDirective)
    app.add_role('acapiobj', AcApiObjRole())
    app.add_node(acapiobj,
        html=(visit_acapiobj_node, depart_acapiobj_node),
        latex=(visit_acapiobj_node, depart_acapiobj_node),
        text=(visit_acapiobj_node, depart_acapiobj_node))
    app.add_object_type('acapiobj', 'acapiobj')


