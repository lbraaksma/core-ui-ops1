
from sphinx.roles import XRefRole
from sphinx import addnodes
from sphinx.locale import _, l_
from docutils import nodes
from sphinx.domains.std import EnvVarXRefRole, GenericObject

class acattr(nodes.General, nodes.Element):
    pass

def visit_acattr_node(self, node):
    pass

def depart_acattr_node(self, node):
    pass


class AcAttrRole(XRefRole):
    
    def result_nodes(self, document, env, node, is_ref):
        if not is_ref:
            return [node], []
        varname = node['reftarget']
        tgtid = 'index-%s' % env.new_serialno('index')
        indexnode = addnodes.index()
        indexnode['entries'] = [
            ('single', varname, tgtid, ''),
            ('single', _('ac attribute; %s') % varname, tgtid, '')
        ]
        targetnode = nodes.target('', '', ids=[tgtid])
        document.note_explicit_target(targetnode)
        return [indexnode, targetnode, node], []

class AcAttrDirective(GenericObject):
    indextemplate = l_('ac attribute; %s')

def setup(app):
    app.add_directive('acattr', AcAttrDirective)
    app.add_role('acattr', AcAttrRole())
    app.add_node(acattr,
        html=(visit_acattr_node, depart_acattr_node),
        latex=(visit_acattr_node, depart_acattr_node),
        text=(visit_acattr_node, depart_acattr_node))
    app.add_object_type('acattr', 'acattr')


