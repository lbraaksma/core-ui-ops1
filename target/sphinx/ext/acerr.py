
from sphinx.roles import XRefRole
from sphinx import addnodes
from sphinx.locale import _, l_
from docutils import nodes
from sphinx.domains.std import EnvVarXRefRole, GenericObject

class acerr(nodes.General, nodes.Element):
    pass

def visit_acerr_node(self, node):
    pass

def depart_acerr_node(self, node):
    pass


class AcErrRole(XRefRole):
    
    def result_nodes(self, document, env, node, is_ref):
        if not is_ref:
            return [node], []
        varname = node['reftarget']
        tgtid = 'index-%s' % env.new_serialno('index')
        indexnode = addnodes.index()
        indexnode['entries'] = [
            ('single', varname, tgtid, ''),
            ('single', _('ac error; %s') % varname, tgtid, '')
        ]
        targetnode = nodes.target('', '', ids=[tgtid])
        document.note_explicit_target(targetnode)
        return [indexnode, targetnode, node], []

class AcErrDirective(GenericObject):
    indextemplate = l_('ac error; %s')

def setup(app):
    app.add_directive('acerr', AcErrDirective)
    app.add_role('acerr', AcErrRole())
    app.add_node(acerr,
        html=(visit_acerr_node, depart_acerr_node),
        latex=(visit_acerr_node, depart_acerr_node),
        text=(visit_acerr_node, depart_acerr_node))
    app.add_object_type('acerr', 'acerr')


