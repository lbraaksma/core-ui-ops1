
from sphinx.roles import XRefRole
from sphinx import addnodes
from sphinx.locale import _, l_
from docutils import nodes
from sphinx.domains.std import EnvVarXRefRole, GenericObject

class acfn(nodes.General, nodes.Element):
    pass

def visit_acfn_node(self, node):
    pass

def depart_acfn_node(self, node):
    pass


class AcFnRole(XRefRole):
    
    def result_nodes(self, document, env, node, is_ref):
        if not is_ref:
            return [node], []
        varname = node['reftarget']
        tgtid = 'index-%s' % env.new_serialno('index')
        indexnode = addnodes.index()
        indexnode['entries'] = [
            ('single', varname, tgtid, ''),
            ('single', _('ac function; %s') % varname, tgtid, '')
        ]
        targetnode = nodes.target('', '', ids=[tgtid])
        document.note_explicit_target(targetnode)
        return [indexnode, targetnode, node], []

class AcFnDirective(GenericObject):
    indextemplate = l_('ac function; %s')

def setup(app):
    app.add_directive('acfn', AcFnDirective)
    app.add_role('acfn', AcFnRole())
    app.add_node(acfn,
        html=(visit_acfn_node, depart_acfn_node),
        latex=(visit_acfn_node, depart_acfn_node),
        text=(visit_acfn_node, depart_acfn_node))
    app.add_object_type('acfn', 'acfn')


