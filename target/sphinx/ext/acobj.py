
from sphinx.roles import XRefRole
from sphinx import addnodes
from sphinx.locale import _, l_
from docutils import nodes
from sphinx.domains.std import EnvVarXRefRole, GenericObject

class acobj(nodes.General, nodes.Element):
    pass

def visit_acobj_node(self, node):
    pass

def depart_acobj_node(self, node):
    pass


class AcObjRole(XRefRole):
    
    def result_nodes(self, document, env, node, is_ref):
        if not is_ref:
            return [node], []
        varname = node['reftarget']
        tgtid = 'index-%s' % env.new_serialno('index')
        indexnode = addnodes.index()
        indexnode['entries'] = [
            ('single', varname, tgtid, ''),
            ('single', _('ac object; %s') % varname, tgtid, '')
        ]
        targetnode = nodes.target('', '', ids=[tgtid])
        document.note_explicit_target(targetnode)
        return [indexnode, targetnode, node], []

class AcObjDirective(GenericObject):
    indextemplate = l_('ac object; %s')

def setup(app):
    app.add_directive('acobj', AcObjDirective)
    app.add_role('acobj', AcObjRole())
    app.add_node(acobj,
        html=(visit_acobj_node, depart_acobj_node),
        latex=(visit_acobj_node, depart_acobj_node),
        text=(visit_acobj_node, depart_acobj_node))
    app.add_object_type('acobj', 'acobj')


