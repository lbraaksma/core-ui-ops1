from sphinx.roles import XRefRole
from sphinx import addnodes
from sphinx.locale import _, l_
from docutils import nodes
from sphinx.domains.std import EnvVarXRefRole, GenericObject

class acop(nodes.General, nodes.Element):
    pass

def visit_acop_node(self, node):
    pass

def depart_acop_node(self, node):
    pass


class AcOpRole(XRefRole):
    
    def result_nodes(self, document, env, node, is_ref):
        if not is_ref:
            return [node], []
        varname = node['reftarget']
        tgtid = 'index-%s' % env.new_serialno('index')
        indexnode = addnodes.index()
        indexnode['entries'] = [
            ('single', varname, tgtid, ''),
            ('single', _('ac operation; %s') % varname, tgtid, '')
        ]
        targetnode = nodes.target('', '', ids=[tgtid])
        document.note_explicit_target(targetnode)
        return [indexnode, targetnode, node], []

class AcOpDirective(GenericObject):
    indextemplate = l_('ac operation; %s')

def setup(app):
    app.add_directive('acop', AcOpDirective)
    app.add_role('acop', AcOpRole())
    app.add_node(acop,
        html=(visit_acop_node, depart_acop_node),
        latex=(visit_acop_node, depart_acop_node),
        text=(visit_acop_node, depart_acop_node))
    app.add_object_type('acop', 'acop')


