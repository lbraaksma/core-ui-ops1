
from sphinx.roles import XRefRole
from sphinx import addnodes
from sphinx.locale import _, l_
from docutils import nodes
from sphinx.domains.std import EnvVarXRefRole, GenericObject

class acprg(nodes.General, nodes.Element):
    pass

def visit_acprg_node(self, node):
    pass

def depart_acprg_node(self, node):
    pass


class AcPrgRole(XRefRole):
    
    def result_nodes(self, document, env, node, is_ref):
        if not is_ref:
            return [node], []
        varname = node['reftarget']
        tgtid = 'index-%s' % env.new_serialno('index')
        indexnode = addnodes.index()
        indexnode['entries'] = [
            ('single', varname, tgtid, ''),
            ('single', _('ac program; %s') % varname, tgtid, '')
        ]
        targetnode = nodes.target('', '', ids=[tgtid])
        document.note_explicit_target(targetnode)
        return [indexnode, targetnode, node], []

class AcPrgDirective(GenericObject):
    indextemplate = l_('ac program; %s')

def setup(app):
    app.add_directive('acprg', AcPrgDirective)
    app.add_role('acprg', AcPrgRole())
    app.add_node(acprg,
        html=(visit_acprg_node, depart_acprg_node),
        latex=(visit_acprg_node, depart_acprg_node),
        text=(visit_acprg_node, depart_acprg_node))
    app.add_object_type('acprg', 'acprg')


