from sphinx.roles import XRefRole
from sphinx import addnodes
from sphinx.locale import _, l_
from docutils import nodes
from sphinx.domains.std import EnvVarXRefRole, GenericObject

class acprop(nodes.General, nodes.Element):
    pass

def visit_acprop_node(self, node):
    pass

def depart_acprop_node(self, node):
    pass


class AcPropRole(XRefRole):
    
    def result_nodes(self, document, env, node, is_ref):
        if not is_ref:
            return [node], []
        varname = node['reftarget']
        tgtid = 'index-%s' % env.new_serialno('index')
        indexnode = addnodes.index()
        indexnode['entries'] = [
            ('single', varname, tgtid, ''),
            ('single', _('ac operation; %s') % varname, tgtid, '')
        ]
        targetnode = nodes.target('', '', ids=[tgtid])
        document.note_explicit_target(targetnode)
        return [indexnode, targetnode, node], []

class AcPropDirective(GenericObject):
    indextemplate = l_('ac property; %s')

def setup(app):
    app.add_directive('acprop', AcPropDirective)
    app.add_role('acprop', AcPropRole())
    app.add_node(acprop,
        html=(visit_acprop_node, depart_acprop_node),
        latex=(visit_acprop_node, depart_acprop_node),
        text=(visit_acprop_node, depart_acprop_node))
    app.add_object_type('acprop', 'acprop')


