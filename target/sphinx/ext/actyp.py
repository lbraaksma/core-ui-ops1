
from sphinx.roles import XRefRole
from sphinx import addnodes
from sphinx.locale import _, l_
from docutils import nodes
from sphinx.domains.std import EnvVarXRefRole, GenericObject

class actyp(nodes.General, nodes.Element):
    pass

def visit_actyp_node(self, node):
    pass

def depart_actyp_node(self, node):
    pass


class AcTypRole(XRefRole):
    
    def result_nodes(self, document, env, node, is_ref):
        if not is_ref:
            return [node], []
        varname = node['reftarget']
        tgtid = 'index-%s' % env.new_serialno('index')
        indexnode = addnodes.index()
        indexnode['entries'] = [
            ('single', varname, tgtid, ''),
            ('single', _('ac type; %s') % varname, tgtid, '')
        ]
        targetnode = nodes.target('', '', ids=[tgtid])
        document.note_explicit_target(targetnode)
        return [indexnode, targetnode, node], []

class AcTypDirective(GenericObject):
    indextemplate = l_('ac type; %s')

def setup(app):
    app.add_directive('actyp', AcTypDirective)
    app.add_role('actyp', AcTypRole())
    app.add_node(actyp,
        html=(visit_actyp_node, depart_actyp_node),
        latex=(visit_actyp_node, depart_actyp_node),
        text=(visit_actyp_node, depart_actyp_node))
    app.add_object_type('actyp', 'actyp')


