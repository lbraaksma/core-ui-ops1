"""
Adds custom highlighting for:
- Formula Engine (fe)
- Mapping Rule Specification language (mrs)
- ACBL (acbl)
"""

def setup(sphinx):
    sphinx.add_lexer("fe", FELexer())
    sphinx.add_lexer("mrs", MRSLexer())
    sphinx.add_lexer("acbl", ACBLLexer())

import re

from pygments.lexer import RegexLexer, bygroups
from pygments.token import *

class FELexer(RegexLexer):
    name = 'Formula Engine'
    aliases = ['fe']
    filenames = ['*.fe']

    flags = re.MULTILINE | re.DOTALL

    keywords = [
        'switch','return','for','else','break','and','if','not','while','default','seq','map','do','case','continue',
        'or']
        
    types = [
        'any','number','integer','float','string','list','date','time','datetime','matrix','opaque']

    builtins = [
        'abs','acos','ado_browse','alpha','alpha_ew','asin','atan','attribute','avg','avg_ew','beta','beta_ew',
        'bsearch','cache_read','cache_write','call','call_function','code','corr','corr_ew','correct_price_field',
        'cos','covar','covar_ew','date','datetime','daydiff','dayminus','dayplus','dload','elt','emavg','evaluate',
        'exec','exit','exp','fill','first','float','floor','fourth','ge_bsearch','ge_lsearch','getenv','getuser',
        'hash_delete','hash_get','hash_global','hash_iskey','hash_keys','hash_local','hash_put','hash_values',
        'indicator.align','indicator.merge','insert','integer','intersect','inverse','is_client','is_date',
        'is_datetime','is_error','is_float','is_function','is_integer','is_list','is_matrix','is_nt_client','is_na',
        'is_number','is_opaque','is_string','is_time','is_us','is_variable','kernel','kernel_info','kurtosis','last',
        'le_lsearch','len','lh','lh_perc','list','list_contents','load','log','lsearch','matrix','matrix.add',
        'matrix.diagmultiply','matrix.eigenvalues','matrix.eigenvectors','matrix.insert','matrix.inverse',
        'matrix.is_positive_definite','matrix.is_symmetric','matrix.multiply','matrix.replace','matrix.select',
        'matrix.transpose','mavg','max','maxobj','mean','min','minobj','name','normaldist','normalize','obj_update',
        'opaque','option.implied_volatility','option.price','original_price_field','out','parallel','pow','price_field',
        'print','random','rdbms_proc','rdbms_proc2','remove','round','second','set_suspect_price_field','sin',
        'skewness','sort','space','split','sql','sqrt','status','stdev','stdev_ew','string','strip','strpos','str',
        'strval','sum','tan','text','text_write','third','threads_info','time','timediff','timeplus','tolower',
        'toupper','transaction_type','transpose','ts','ts_attributes','tsext','ts_update','type','var','var_ew',
        'yearfraction','node','node_query','node_browse','node2symbol','node2path','subnodes']

    def _multi_escape(entries):
        return '(%s)\\b' % ('|'.join(re.escape(entry) for entry in entries))

    tokens = {
        'root': [
            (r'(function\s+)'
             r'([a-z]+\s+)?'                   # optional type, only supported in //@external
             r'([a-zA-Z_][a-zA-Z0-9_\.]*)',    # function name
             bygroups(Keyword.Declaration, Keyword.Type, Name.Function)),
            (r'^\s*//@[a-zA-Z_][a-zA-Z0-9_]*', Name.Decorator),
             (r'[^\S\n]+', Text),
            (r'//.*?\n', Comment.Single),
            (r'/\*.*?\*/', Comment.Multiline),
            (r'(global|local|function)\b', Keyword.Declaration),
            (_multi_escape(keywords), Keyword),
            (_multi_escape(builtins) + '(?=\s*\()', Name.Builtin),
            (_multi_escape(types), Keyword.Type),
            (r'"[^"]*"', String),
            (r"'[^']*'", String),
            (r'[a-zA-Z_][a-zA-Z0-9_\.]*', Name),
            (r'\$[a-zA-Z0-9_]*\.?', Name.Decorator), # dot is to support $SYSTEM.<builtin-function>
            (r'[~\^\*!%&<>\|+=?:\-\/]', Operator),
            (r'[\[\]\(\)\{\};,]', Punctuation),
            (r'(([0-9]+\.[0-9]*)|(\.[0-9]+))([eE][+-]?[0-9]+)?', Number.Float),
            (r'[0-9]+', Number.Integer),
            (r'\n', Text),
        ]
    }

class MRSLexer(RegexLexer):
    name = 'Mapping Rule Specification'
    aliases = ['mrs']
    filenames = ['*.mrs']

    flags = re.MULTILINE | re.DOTALL

    keywords = [
        # 'Not=' is handled hardcoded
        'Package','Setup','Import','Template','Main-package','Fe-template','Rule','Is','End','Where','And','Return','In',
        'Or-else','Or','Let','Be','Then','If','Else']
        
    def _multi_escape(entries):
        return '(%s)\\b' % ('|'.join(re.escape(entry) for entry in entries))

    tokens = {
        'root': [
            (r'[^\S\n]+', Text),
            (r'//.*?\n', Comment.Single),
            (r'/\*.*?\*/', Comment.Multiline),
            (_multi_escape(keywords), Keyword),
            (r'Not=', Keyword),
            (r'"[^"]*"', String),
            (r"'[^']*'", String),
            (r'`[^`]*`', Name.Decorator),                        # PSEUDO_EXPRESSION
            (r'%[a-zA-Z0-9_-]+', Name.Variable),                 # PARAM_ID
            (r'[a-z][a-z0-9-]*', Name.Text),                     # WORD
            (r'[A-Z][A-Z0-9_#\+\.]*[A-Z0-9]', Name.Label),       # ATTRIBUTE_ID
            (r'[A-Z0-9_][a-zA-Z0-9_]*', Name.Label),             # MAPPING_ID
            (r'[a-z][a-z0-9-]*(\.[a-z][a-z0-9-]*)*', Name.Text), # PACKAGE_ID
            (r'[~\^\*\/!%&<>\|+=?:-]', Operator),
            (r'[\[\]\(\)\{\};,]', Punctuation),
            (r'(([0-9]+\.[0-9]*)|(\.[0-9]+))([eE][+-]?[0-9]+)?', Number.Float),
            (r'[0-9]+', Number.Integer),
            (r'\n', Text),
            (r'(@)(\s*)([a-z][a-z0-9-]*)',
             bygroups(Name.Operator, Text, Name.Label))
        ]
    }

from pygments.lexers.sql import PostgresLexer

class ACBLLexer(RegexLexer):
    name = 'ACBL'
    aliases = ['acbl']
    filenames = ['*.acbl']

    #flags = re.MULTILINE | re.DOTALL

    keywords = [
        'TRANS', 'NOLOG', 'NOBATCHES', 'NOFAIL', 'CASCADE', 'NO_CASCADE', 'FORCE_TRIG_INTER', 'DO_TRIG_INTER', 'NO_TRIG_INTER', 'DO_TRIG_INTRA', 'NO_TRIG_INTRA']
    
    # SQL specific
    sql_keywords = ['insert','update','delete','into','values', 'TABLE', 'ROW', 'ALT', 'NULL']

    sql_types = ['INTEGER', 'FLOAT', 'DATETIME', 'OPAQUE', 'STRING']

    # Object ACBL specific
    objects = ['ADO', 'ADO_CODE', 'ADO_ATTRIBUTE', 'LIST', 'DERIVED_LIST', 'LIST_CONTENTS', 'CURVE', 'DERIVED_CURVE', 'CURVE_CONTENTS', 'INTERFACE', 'INTERFACE_PARAMETER',
               'INTERFACE_MAPPING', 'FORMULA', 'CALENDAR', 'HOLIDAY', 'TREE', 'STATICATTRIBUTE', 'STATICATTRIBUTE_ENUM', 'STATICATTRIBUTE_CODE', 'STATICATTRIBUTE_DEP',
               'DATAFILEATTRIBUTE','DATAFILEATTRIBUTE_CODE', 'USER', 'USER_GROUP', 'USER_ROLE', 'GROUP', 'ROLE', 'ROLE_EXPR', 'ATTRIBUTE_FORMULA']
    operations = ['INSERT','UPDATE','ADD','DELETE','TRUNCATE','TRIGGER','RENAME']

    def _multi_escape(entries):
        return '(%s)\\b' % ('|'.join(re.escape(entry) for entry in entries))

    tokens = {
        'root': [
            (r'[^\S]+', Text),
            (_multi_escape(keywords), Keyword),
            (r'SQL', Keyword, 'sql'),
            (r'OBJECT', Keyword, 'object'),
            (r'(STP)(\s+.*)',
              bygroups(Keyword, Text))
        ],
        'object' : [
            (r'OBJECT', Keyword),
            (r'TRANS', Keyword, '#pop'),
            (r'[^\S]+', Text),
            (r'\!?"[^"]*"', String), # !".." is only valid actually for compound attribute values like value=!"[1,2]"
            (r'0x[0-9a-f]+', Number.Hex),
            (r'[0-9]{8}:[0-9]+', Literal.Date),
            (r'(([0-9]+\.[0-9]*)|(\.[0-9]+))([eE][+-]?[0-9]+)?', Number.Float),
            (r'-?[0-9]+', Number.Integer),
            (_multi_escape(objects), Name.Class),
            (_multi_escape(operations), Name.Decorator),
            (r'([a-z]+)(\s*=)', 
             bygroups(Name.Attribute, Text)),
        ],
        'sql' : [
            (r'SQL', Keyword),
            (r'TRANS', Keyword, '#pop'),
            (r'[^\S]+', Text),
            (r"'[^']*'", String),
            (r'"[^"]*"', String),
            (r'0x[0-9a-f]+', Number.Hex),
            (r'[0-9]{8}:[0-9]+', Literal.Date),
            (r'(([0-9]+\.[0-9]*)|(\.[0-9]+))([eE][+-]?[0-9]+)?', Number.Float),
            (r'-?[0-9]+', Number.Integer),
            (_multi_escape(sql_keywords), Keyword),
            (_multi_escape(sql_types), Keyword.Type),
            (r'[a-zA-Z][a-zA-Z0-9_]*', Name),
            (r'[\(\),\?\*=]', Punctuation),
        ]
    }
