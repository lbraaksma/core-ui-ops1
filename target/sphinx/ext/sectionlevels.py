from sphinx.writers.latex import LaTeXTranslator

def setup(sphinx):
	LaTeXTranslator.sectionnames =  ["part", "section", "subsection",
									 "subsubsection", "paragraph", "subparagraph"]