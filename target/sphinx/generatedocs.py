##
# Generate documentation from rst
#
# - Usually run by maven
# - Creates HTML, PDF (A4) and PDF (US letter) documents
# - If aspell is available, also builds singlehtml format and spell checks it
# - Checks writing style
# - Copies (with rsync) the _build/html directory to techdoc.asset-control.com
# - Zips HTML and adds to downloads list
# - Writes a warranty page to be appended to PDFs
# - Writes an HTML info page that can be used to set the Jenkins project description
#   using the 'Project Description Setter' plugin
#
import os, sys, subprocess, platform, tempfile, glob, shutil, re, socket
import getopt
import posixpath # Needed to ensure paths for download PDFs are in linux format
import properties # Sphinx properties.py file
import datetime, time

global logdir, system, name, sphinxbuildexe, sphinxbuildopts, aspellexe, w3mexe, styleexe
global spellcmd, pdflatexexe, man_pages, allowed_pub_hosts
global pdf_filenames
pdf_filenames = {} # Used to make downloads.rst, Dictionary of 'papersize': ['path','path',...]
global html_filenames
html_filenames = {} # Also for downloads.rst, list of compressed HTML archives. Dictionary of 'theme': ['path','path',...]

##
# Values for replacement into warranty file
#
company='Asset Control International B.V.'
shortcompany='Asset Control'
startyear='2011'
endyear=datetime.datetime.now().year
web_company='www.asset-control.com' # 'http://' is added in template
email_documentation='docs@asset-control.com'
email_general='info@asset-control.com'
email_training='training@asset-control.com'

# TODO put these in protected file or maven envvar
# Current 'sphinx' labelled build servers
allowed_pub_hosts = ['build-generic-01','build-generic-02','buildvm09','buildvm12','buildvm13','bamboo-prod']

# Sleep for this many seconds after builds
sleep_time = 5

##
# Get used values from properties.py
#

# Check whether man_pages is defined - calling man builder without it causes error
man_pages = False
if hasattr(properties, 'man_pages'):
    man_pages = True

# Option to skip spell checking
skip_spell_check = False
if hasattr(properties, 'skip_spell_check'):
    skip_spell_check = True

project = ''
if hasattr(properties, 'project'):
    project = properties.project


# Check for latex properties. If present, make list of PDF file-names based
# on .tex entries for creating dynamic downloads.rst page.
build_latex = False
if (hasattr(properties, 'latex_documents') and len(properties.latex_documents) > 0):
    build_latex = True


##
# Components for Project Description paths/URLs
#
maven_root = ''

if 'JOB_URL' in os.environ: # On jenkins
   maven_root = os.path.join(os.environ['JOB_URL'], 'ws','target', os.path.basename(os.getcwd()))
else: # Local
   maven_root = os.getcwd()

project_root = os.path.basename(maven_root)

root       = ''
logdir     = '_logs'
system     = platform.system()
name       = os.name
host       = socket.gethostname()

##
# Publish details for HTML output
#
web_module     = 'webdoc' # Default module name in /etc/rsyncd.conf on web host
web_dir        = '' # No default, this is the directory, e.g. ac-server_7-3
if hasattr(properties, 'publish_to'):
    (web_module, web_dir) = properties.publish_to

web_host       = 'techdoc.asset-control.com'
web_user       = 'librarian'
web_pass_file  = '.webpass'
web_src        = '_build/html/' # What to copy to web server - trailing slash is required (see man page for rsync)

##
# Downloadable HTML name suffixes
#
doc_sfx = '_techdoc'
zip_sfx = '.zip'
tgz_sfx = '.tgz'

##
# Files written by this script
#
fn_download = 'downloads.rst'
fn_warranty = 'warranty' # output based on template
fn_warranty_template = 'warranty-template'
jenkins_project_description = 'jenkins-project-description.html' # Project summary info read by 'Project Description Setter' plugin
global jenkins_info # File handle for above

###
# Executables
#
aspellexe      = 'aspell'
w3mexe         = 'w3m' # Text-based browser used to convert HTML to text for readability metrics of GNU 'style'
styleexe       = 'style'
sphinxbuildexe = 'sphinx-build'
pdflatexexe    = 'pdflatex'
rsyncexe       = 'rsync'
tarexe         = 'tar'
jarexe         = 'jar'
#findexe        = 'find' # Not available on windows, use grep | sed instead
grepexe        = 'grep'
sedexe         = 'sed'
rmexe          = 'rm'

##
# Executable options
#
sphinxbuildopts = ['-n','-N','-v','-v','-a','-d',os.path.join( '_build', 'doctrees')]
# Dynamically add options to sphinx-build to switch between RTD and basic HTML theme
# for themed and plain HTML download.
sphinxRTDhtmltheme = ['-D','html_theme=sphinx_rtd_theme','-D','html_theme_path=_themes','-D','html_theme_options.display_version="True"']
sphinxbasichtmltheme = ['-D','html_theme=basic','-D','html_theme_options={}']
pdflatexopts    = ['-halt-on-error', '-file-line-error', '-interaction=batchmode']
# pdflatexopts += ['-shell-escape'] # testing on large tables (5000+ rows)
rsyncopts       = ['--delete', '--recursive', '--compress', '--chmod=Du=rwx,Dgo=rx,Fu=rw,Fog=r', '--cvs-exclude', '--exclude', 'docs']
# rsyncopts       = ['--recursive', '--compress', '--cvs-exclude']
taropts         = ['-c','-z','-C','_build','-f']
#jaropts         = ['-C','_build',] # jar options are less flexible so additional ones are added at the time of invocation in compressHTML

# WARNING: aspell options are different between 0.50 and 0.60
aspellopts      = ['list','--home-dir=.']
# Filters - specified in .aspell.conf

w3mopts = ['-dump','-T','text/html']
styleopts = [] # TESTING - None yet
grepopts = ['-r','-l',web_host]
rmopts = ['-r','-f']

##
# adjust for windows
#
if name == 'nt':
    sphinxbuildexe += '.exe'
    pdflatexexe    += '.exe'
    aspellexe      += '.exe'
    w3mexe         += '.exe'
    styleexe       += '.exe'
    rsyncexe       += '.exe'
    tarexe         += '.exe'
    jarexe         += '.exe'
    grepexe        += '.exe'
    sedexe         += '.exe'
    rmexe          += '.exe'

spellcmd       = [aspellexe] + aspellopts
w3mcmd         = [w3mexe] + w3mopts
stylecmd       = [styleexe] + styleopts
pdflatexcmd    = [pdflatexexe] + pdflatexopts
rsynccmd       = [rsyncexe] + rsyncopts
tarcmd         = [tarexe] + taropts
# jarcmd         = [jarexe] + jaropts # See compressHTML()

# to replace URL in downloaded HTML in compressHTML()
grepcmd = [grepexe] + grepopts
# Used to remove unwanted directories in compressHTML()
dirstodelete = ['_sources']
rmcmd = [rmexe] + rmopts

##
# Check if dir present & writable
#
def dirOK(dirname):
    if os.access(dirname,os.F_OK) and os.access(dirname,os.W_OK):
        return True
    return False

##
# Check/make a separate log dir
#
def make_logdir(ld):
    if dirOK(ld):
        print('Log directory ' + ld + ' already present and writable')
    elif not os.access(ld,os.F_OK):
        try:
            print("Creating log directory " + ld)
            os.mkdir(ld)
        except OSError:
            print("Can't create log directory " + ld + ', falling back to ' + root)
            ld = root
    elif not os.access(ld,os.W_OK):
        print("Log directory " + ld + " not writable, falling back to " + root)
        ld = root
    else:
        ld = root
    return ld

##
# Check for windows/linux program
#
def which(program):
    def is_exe(fpath):
        return os.path.isfile(fpath) and os.access(fpath, os.X_OK)

    fpath, fname = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            path = path.strip('"')
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file
    return None

##
# Call command, send stderr/stdout to logdir/logfile
#
def call_log(command, logdir, logfile):
    logpath = os.path.join(logdir,logfile)
    logto = open(logpath, 'a')
    print('Running: %s, logging to %s' % (' '.join(command), logto.name))
    sys.stdout.flush() # prevent inconsistent ordering of print statements and command output
    p1 = subprocess.Popen(command, stdout=logto, stderr=subprocess.STDOUT)
    retcode=p1.wait()

    if retcode != 0:
        raise Exception("Command %s failed with retcode %s" % (command, retcode))
    logto.close()
    return retcode

##
# Call command, log to stdout
#
def call(command,logme=True):
    if logme:
        print('Running: %s' % ' '.join(command))
    sys.stdout.flush() # prevent inconsistent ordering of print statements and command output
    p1 = subprocess.Popen(command, stderr=subprocess.STDOUT)
    retcode=p1.wait()

    if retcode != 0:
        raise Exception("Command %s failed with retcode %s" % (command, retcode))
    return retcode

##
# Run a command, specifying in, out and stderr files
#
def run(command, infile, outfile, errfile):
    in_f  = None if infile  == None else open(infile,'r')
    out_f = None if outfile == None else open(outfile,'w')
    err_f = None if errfile == None else open(errfile, 'w')
    print('Running: %s' % (' '.join(command)))
    sys.stdout.flush()
    p1 = subprocess.Popen(command, stdin=in_f, stdout=out_f, stderr=err_f)
    retcode=p1.wait()

    if infile != None:
        in_f.close()

    if outfile != None:
        out_f.close()

    if errfile != None:
        err_f.close()

    return retcode

##
# Make sure sphinx is installed
#
def check_sphinx():
    if which(sphinxbuildexe) == None:
        print('Sphinx not installed - exiting')
        exit(0)

##
# Build sphinx for non-PDF types
# typ: html | singlehtml | man
# where: Optional output path, defaults to typ
#
def build(typ, where, extraopts=[]):
    out = [os.path.join('_build',where)]
    fmt = ['-b',typ]
    cmd = [sphinxbuildexe] + ['-w', os.path.join(logdir,'sphinx-build-warnings-%s.log' % typ)] + sphinxbuildopts + extraopts + fmt + ['.'] + out
    call_log(cmd, logdir, 'sphinx-build-stdout-%s.log' % typ)
    time.sleep(sleep_time) # Time for sphinx processes to finish

##
# Build PDFs
#
def build_pdf(typ,papersize='a4'):
    fmt = ['-b',typ]
    fmt += ['-t',typ.upper()] # Add tag needed by conf.py replacements

    if papersize == 'usletter':
        fmt += ['-D','latex_elements.papersize=letterpaper']
        typ += '_' + papersize # Add suffix to output path for US letter

    out = [os.path.join('_build',typ)]

    cmd = [sphinxbuildexe] + ['-w', os.path.join(logdir,'sphinx-build-warnings-%s.log' % typ)] + sphinxbuildopts + fmt + ['.'] + out
    call_log(cmd, logdir, 'sphinx-build-stdout-%s-%s%s' % (typ, papersize, '.log'))

    if which(pdflatexexe) == None:
        print('pdflatex not installed - unable to create PDF from .tex files')
    else: # iterate over all tex files and generate pdf
        print('Processing files in %s' % out[0])
        pop = os.getcwd()
        os.chdir(out[0])

        pdf_match = re.compile(r".tex$")
        pdf_filenames[papersize] = []
        for latexFile in glob.glob("*.tex"):
            print("Processing %s" % latexFile)
            # Make list of .tex filenames changed to .pdf extension
            # Used for creating downloads.rst
            pdf_filenames[papersize] += [posixpath.join('_build', typ, pdf_match.sub(".pdf",latexFile))]

            # IMPORTANT NOTE: This many runs of latex are necessary to correctly
            # process links and ToC entries. Any fewer results in inconsistent
            # references.
            latexcommand = pdflatexcmd + [latexFile]
            call_log(latexcommand, '.', latexFile + '.pdflatex-1-' + papersize + '.log')
            call_log(latexcommand, '.', latexFile + '.pdflatex-2-' + papersize + '.log')

            basename, extension = os.path.splitext(latexFile)
            call_log(["makeindex", "-s", "python.ist", "%s.idx" % basename], '.', latexFile + '_makeindex-' + papersize + '.log')
            call_log(latexcommand, '.', latexFile + '.pdflatex-3-' + papersize + '.log')
            call_log(latexcommand, '.', latexFile + '.pdflatex-4-' + papersize + '.log')
            call_log(latexcommand, '.', latexFile + '.pdflatex-5-' + papersize + '.log')
        os.chdir(pop)

##
# Perform a manual pipe line to progressively post-process the aspell output
#
# Notes:
# - Python's pipes module doesn't work on windows - it quotes paths.
# - Other attempts with subprocess.PIPE resulted in empty files.
# - Current file chain approach works and is easier to debug/monitor.
# - Linux vs Windows aspell (v0.60 vs v0.50) have different options.
#
# See also readability_check() that uses a similar approach
#
def spell_check():
    step = 0
    fnam = 'aspell.'
    fout = '.out'
    ferr = '.err'

    # Run spell check
    i = os.path.join( "_build", "singlehtml", "index.html")
    o = os.path.join(logdir, fnam + str(step) + fout)
    e = os.path.join(logdir, fnam + str(step) + ferr)
    r = run(spellcmd,i,o,e)

    # Sort remaining words
    step += 1
    cmd = ['sort']
    i = o
    o = os.path.join(logdir, fnam + str(step) + fout)
    e = os.path.join(logdir, fnam + str(step) + ferr)
    r = run(cmd,i,o,e)

    # Count word total
    step += 1
    cmd = ['uniq','--count']
    i = o
    o = os.path.join(logdir, 'spell-check.txt')
    e = os.path.join(logdir, fnam + str(step) + ferr)
    r = run(cmd,i,o,e)

    # Count word frequencies
    num_lines = sum(1 for line in open(o))

    # Report to log and project info file
    msg1 = 'Spell check: %d unknown words (see %s).' % (num_lines, o)
    print(msg1)

    msg2 = 'Spell check: <a href="%s">%d unknown words</a>' % (os.path.join(maven_root, o), num_lines)
    jenkins_info.write('<p>' + msg2 + '</p>')

##
# Convert single HTML output to plain text and get basic readability metrics
#
def readability_check():
   step = 0
   fnam = 'style.'
   fout = '.out'
   ferr = '.err'

   # Convert to text
   i = os.path.join( "_build", "singlehtml", "index.html")
   o = os.path.join(logdir, fnam + str(step) + fout)
   e = os.path.join(logdir, fnam + str(step) + ferr)
   r = run(w3mcmd,i,o,e)

   # Run style - TODO: Test options
   step += 1
   i = o
   o = os.path.join(logdir, 'style-check.txt')
   e = os.path.join(logdir, fnam + str(step) + ferr)
   r = run(stylecmd,i,o,e)

##
# Check if publish is allowed from this build hosts (plus host.asset-control.com)
#
def pubauth(hostname):
    if hostname in allowed_pub_hosts + [x + '.asset-control.com' for x in allowed_pub_hosts]:
        return True
    return False

##
# Copy to web server (WIP)
# TODO implement ssh
#
def publish(module,directory):
    if not module or not directory:
        print('Web publish module or directory empty - skipping publish of HTML. (Check value of publish_to in properties.py)')
        return
    print('Publishing contents of %s to http://%s' % (web_src, web_host))
    cmd = rsynccmd + ['--password-file=%s' % web_pass_file, web_src, '%s@%s::%s/%s' % (web_user, web_host, web_module, web_dir)]
    call(cmd)

##
# Create a human readable string representation of a file size value
# See:
# https://stackoverflow.com/questions/5194057/better-way-to-convert-file-sizes-in-python
#
def GetHumanReadable(size,precision=2):
    suffixes=['B','KB','MB','GB','TB']
    suffixIndex = 0
    while size > 1024 and suffixIndex < 4:
        suffixIndex += 1 #increment the index of the suffix
        size = size/1024.0 #apply the division
    return "%.*f %s"%(precision,size,suffixes[suffixIndex])

##
# Create a downloads.rst file containing download links to downloadable items (PDF and compressed HTML).
# NOTE: Automatically added to HTML pages - no need to include in toctree (See ac-doc/src/main/sphinx/_templates/layout.html)
#
def downloads_pdf(f):
    df = open(fn_download, 'w')
    df.write('#########\nDownloads\n#########\n\n')
    for sz in sorted(f.keys()):
        sz_txt = sz[:2].upper() # Reuse key: 'A4' from 'a4', 'US' from 'usletter'
        df.write('**PDF (%s format)**\n\n' % sz_txt)
        for pdf in sorted(f[sz]):
            # Use posixpath as the download links will be copied to Linux
            df.write('* :download:`' + os.path.basename(pdf) + '<' + pdf + '>` (' + GetHumanReadable(os.path.getsize(pdf)) + ')\n\n')
    df.close()

##
# Append to the downloads.rst file compressed HTML archives for RTD and plain theme
#
def downloads_html(f):
    df = open(fn_download, 'a')

    for theme in sorted(f.keys()):
      df.write('\n**HTML (%s)**\n\n' % theme)

      for archive in sorted(f[theme]):
         extension = archive.split('.')[-1]
         df.write('* :download:`' + project + ' Documentation (' + extension + ') <' + archive + '>` (' + GetHumanReadable(os.path.getsize(archive)) + ')\n')

    df.write('\n\n.. tip::\n\n')
    df.write('   * HTML is provided in themed and plain versions.\n')
    df.write('   * Download and unpack your choice of archive according to your platform:\n\n')
    df.write('     - ``tgz`` is a tar/gzip archive for UNIX and Linux.\n')
    df.write('     - ``zip`` for MS Windows.\n\n')
    df.write('   * In the expanded folder, open the ``index.html`` file in a web browser.\n\n')
    df.close()


##
# Write warranty page
# Uses template and str.format() with replacements are top of file.
#
def warranty():
    print('Writing warranty page')
    dfw = open(fn_warranty, 'w')
    dfr = open(fn_warranty_template, 'r')
    dfw.write(dfr.read().format(company=company,
                                shortcompany=shortcompany,
                                startyear=startyear,
                                endyear=endyear,
                                web_company=web_company,
                                email_documentation=email_documentation,
                                email_general=email_general,
                                email_training=email_training))
    dfr.close()
    dfw.close()

##
# Compress the HTML directory so it can be included in the download list and
# downloaded for local browsing.
# Assumes a duplicate HTML build has been made excluding download links.
# Create both tar/gzip and jar (zip) archives.
# theme: rtd | plain
#
def compressHTML(build_dir, theme):
   html_filenames[theme] = []
   real_dir = os.path.join("_build", build_dir) # Relative to target/<sphinx project folder>
   if not dirOK(real_dir): # Only if dir exists (i.e. html build was invoked for this project)
      return False

   # Do some cleaning up of HTML first:
   # a) Remove unneeded directories - reduces size and removes text versions of RST files
   #    which may contain comments or internal info not meant for public viewing.
   # b) Replace techdoc URL - don't want local copies linking to online version.

   # a)
   for d in dirstodelete:
      call(rmcmd + [os.path.join(real_dir,d)])

   # b) Windows has no find so can't use traditional 'find -exec sed' approach
   ## Step 1 - make list of files to be edited
   pipe_cmd_a = grepcmd + [posixpath.join('_build',build_dir)] # Using posix here due to quirk of grep where output has mixed path separators (both forward and backward slashes)
   stage = "grep"
   fout = '.out'
   ferr = '.err'
   step = 0
   i = None
   o = os.path.join(logdir, stage + str(step) + fout)
   e = os.path.join(logdir, stage + str(step) + ferr)
   r = run(pipe_cmd_a,i,o,e)

   ## Step 2 - For each file listed, replace web site with null
   sedspec = 's/href="http:\/\/%s"//g' % web_host
   df = open(os.path.join(logdir,'replace_host.sed'), 'w')
   df.write(sedspec)
   df.close()
   sedopts = ['--in-place','--file=%s' % os.path.join(logdir,'replace_host.sed')]
   sedcmd  = [sedexe] + sedopts

   sys.stdout.write('Post-processing distributable HTML files')
   for f in open(o):
      sys.stdout.write('.')
      call(sedcmd + [f.rstrip()],False) # Too many to log
   sys.stdout.write('\n')

   # Create archives
   call(tarcmd + [build_dir + tgz_sfx ,build_dir]) # opts include '-C _build' so that dir unpacks with name of project
   html_filenames[theme] += [build_dir + tgz_sfx]
   jarcmd = [jarexe,'cMf',build_dir + zip_sfx,'-C','_build/' + build_dir,'.']
   call(jarcmd)
   html_filenames[theme] += [build_dir + zip_sfx]
   return True # TODO check jar/tar file, add checksum value

##
# main()
#
if __name__ == '__main__':
   # Initialisation
   print('Building documents for [' + project + ']')
   quick_build = False
   try:
      opts, args = getopt.getopt(sys.argv[1:],"q")
   except getopt.GetoptError:
      print('Usage: generatedocs.py [-q|--quick-build]')
      sys.exit(2)
   for opt,val in opts:
      if opt == '-q':
         quick_build = True
   print('Quick build is %s' % quick_build)

   # Check, make logdir
   check_sphinx()
   logdir = make_logdir(logdir)
   # print('OS is ' + name)
   # print('Platform is ' + system)
   print('Logging to ' + logdir)

   # Write an info file to be used by Jenkins 'Project Description Setter' plugin
   jenkins_info = open(os.path.join(logdir, jenkins_project_description),'w')
   jenkins_info.write('<p style="color:red"><b>NOTE:</b> This description text was generated by %s and inserted by Jenkins <i>Project Description Setter</i> plugin.</p>' % __file__)
   jenkins_info.write('<h3>Sphinx-generated Documentation for %s</h3>\n<ul>\n' % project)
   # NOTE: 'docs' in URL is mapped to 'webdoc' (rsync module) in Apache server config.
   if web_dir: # Don't link to root
      jenkins_info.write('<li><a href="http://%s/docs/%s">Online</a></li>\n' % (web_host, web_dir))
   jenkins_info.write('<li><a href="%s">Local</a></li>\n</ul>\n\n' %  os.path.join(maven_root,'_build'))

   # main
   warranty() # Page added to PDFs - Always need this

   if quick_build: # A4 PDFs only
      build_pdf('latex')
   else: # Everything
      if not skip_spell_check:
         if which(aspellexe) != None:
            print('%s found - generating single HTML output for spell checking' % aspellexe)
            build('singlehtml','singlehtml')
            spell_check()
            if which(w3mexe) and which(styleexe):
               readability_check()

      if man_pages:
         build('man','man')

      build_pdf('latex') # A4
      # US format removed on request of Ronald to reduce time for Bloomberg build to complete.
      # Also currently no US sized PDF being copied to userweb.
      # build_pdf('latex','usletter')

      # Separate HTML build for downloading.
      # This is because a valid downloadable html.tar.gz file is needed
      # for the downloads.rst file - which is processed by the HTML sphinx build.
      # To get out of this chicken/egg situation, a separate HTML sphinx build
      # is made. This does not include the _downloads directory created by
      # sphinx whenever a :downloads: role is found. This build is tar/gzipped
      # and used as the downloadable HTML source.
      build('html',os.path.join(os.path.dirname('project'),doc_sfx),sphinxRTDhtmltheme)
      compressHTML(os.path.dirname('\"'+os.path.join(project,doc_sfx)+'\"'), 'Themed') # Create downloadable HTML archives

      # No theme HTML - for customising or including in own docs - also much smaller download size
      build('html',os.path.join(os.path.dirname('project'),doc_sfx,'_plain'),sphinxbasichtmltheme)
      compressHTML(os.path.dirname('\"'+os.path.join(project,doc_sfx)+'\"'), 'Plain') # Create downloadable HTML archives

      downloads_pdf(pdf_filenames) # Creates downloads.rst and adds PDFs
      downloads_html(html_filenames) # Appends HTML downloads
      build('html','html',sphinxRTDhtmltheme) # The unmodified HTML used for web site (techdoc)

      if pubauth(host):
         publish(web_module, web_dir)
      else:
         print("Hostname %s not in list to publish HTML" % host)

   jenkins_info.close()
   exit(0)
