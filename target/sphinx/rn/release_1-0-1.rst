####################
1.0.1 (October 2019)
####################

*******
Summary
*******

|rn-new| 0 `New`_

|rn-fixed| 1 `Fixed`_

|rn-updated| 0 `Updated`_

*************
Install Notes
*************

A new column needs to be added to the cassandra table:

.. code-block:: console

   ALTER TABLE lineage.attribute_change ADD lineage_addition text;

************
Known issues
************

==============
Authentication
==============

There is at the moment no authentication. Any user can retrieve and store lineage information. 

***
New
***

None.


*****
Fixed
*****

.. DL-89: Cassandra Insert Timeouts

=========================================
Cassandra WriteTimeoutException on INSERT
=========================================

WriteTimeoutExceptions related to failures with Cassandra Lightweight Transactions were observed.
The way data is written into the cassandra table has been modified to not require Lightweight Transactions.
This has performance benefits and also avoids these `WriteTimeoutException` exceptions.

**Reference:** DL-89


*******
Updated
*******

None.
