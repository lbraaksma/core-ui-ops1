####################
1.0.2 (October 2019)
####################

*******
Summary
*******

|rn-new| 0 `New`_

|rn-fixed| 6 `Fixed`_

|rn-updated| 3 `Updated`_

*************
Install Notes
*************

A new column needs to be added to the Cassandra table:

.. code-block:: console

   ALTER TABLE lineage.attribute_change ADD user_id text;

************
Known issues
************

==============
Authentication
==============

There is at the moment no authentication. Any user can retrieve and store lineage information.

***
New
***

None.


*****
Fixed
*****

.. DL-104: Ingestion Service Hang

============================================================
Ingestion Service validation of large JSON data is very slow
============================================================

The Ingestion Service validates the JSON structure of incoming lineage data.
A bug in this process caused the validation to take a very long time, giving the appearance that the Ingestion Service was hanging.
This was also causing timeout error and IO Exception errors on the Propagation Service side.

**Reference:** DL-104

==========================================
Configurable Cassandra reconnection policy
==========================================

The Cassandra reconnection policy is now configurable using environment variables. Please read
the ``Installation`` section of the Administration Guide for more details.

.. DL-95: Services should be able to start if Casssandra is not reachable.

=====================================
Retry Cassandra connection on startup
=====================================

The Ingestion and Read Services no longer exit if Cassandra is not
reachable on startup. Instead they will keep trying to connect using the configurable reconnection 
policy.

**Reference:** DL-95

.. DL-101: Change default max retries

===================================================
Default MAX_RETRIES changed for Propagation Service
===================================================

The default value for configurable option MAX_RETRIES has
been changed from "infinite" to 10. This prevents threads
from endlessly retrying failing files.

**Reference:** DL-101

.. DL-97: Propagation Service should immediately fail any submission that cannot succeed

=================================================
Propagation Service should not retry in all cases
=================================================

Retries are only attempted for certain error codes returned
by the ingestion service to prevent threads from retrying
files that will never succeed.

**Reference:** DL-97

.. DL-92: Docker image and configuration improvements

======================================================
Failure directory configurable for Propagation Service
======================================================

A "failure directory" can now be configured for where to move files to
that have errors reported by the Ingestion Service.
To use this directory you can bind mount a docker volume to 
``/opt/ac/lineage_fails`` inside the docker container.
By default this directory is not used and files are not moved.

**Reference:** DL-92

*******
Updated
*******

.. DL-90: Use openjdk base for docker images

=======================================
Docker image size significantly reduced
=======================================

The docker images have been reduced in size:

- The Ingestion Service from about 775MB to 298MB.
- The Read Service from about 694MB to 298MB.
- The Propagation Service from about 600MB to 93MB.

**Reference:** DL-90, DL-92

.. DL-92: Docker image and configuration improvements

===============          
HTTPS supported
===============

The Read and Ingestion Service now support https. Pleas read the ``installation`` section
of the Administration Guide on how to enable this.

**Reference:** DL-92

.. DL-105: Store and return userId along with sourceName

=========================================
New lineage property ``userId`` supported
=========================================

Since AC Server 7.4.1, a new property ``userId`` was added to the lineage. 
This property was added to not only be able to store the regular AC Plus user, which is provided
through ``sourceName``, but also an optional non-AC user that used the AC Plus user (e.g. acdba)
on its behalf for doing the operations.

The Ingestion Service now supports storing the value for this property by 
writing it to the new Cassandra column ``user_id``. 

.. note:
   
   For existing Data Lineage installations, this new column needs to be added manually. 
   Please see section  ``Installation Notes`` on how to do this.

The Read Service returns both properties: ``sourceName`` and ``userId``. 
If ``userId`` is null, it will get the value of ``sourceName``.

**Reference:** DL-105
