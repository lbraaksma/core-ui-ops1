#####################
1.0.3 (November 2019)
#####################

*******
Summary
*******

|rn-new| 1 `New`_

|rn-fixed| 0 `Fixed`_

|rn-updated| 1 `Updated`_

*************
Install Notes
*************

A new table needs to be added to the ``lineage`` keyspace in Cassandra:

.. code-block:: console

    CREATE TABLE lineage.attribute_correction (
        object_id text,
        attribute_id text,
        applies_at timestamp,
        recorded_on timestamp,
        datamodel_id text,
        class_id text,
        lineage text,
        lineage_addition text,
        transaction_id text,
        process_id text,
        source_id bigint,
        source_name text,
        user_id text,
        comment text,
        PRIMARY KEY ((object_id, attribute_id), applies_at, recorded_on)
    )
    WITH CLUSTERING ORDER BY (applies_at DESC, recorded_on DESC);

************
Known issues
************

==============
Authentication
==============

There is at the moment no authentication.

***
New
***

..  DL-119: Return correction records

==========================================
Support data lineage for correction values
==========================================

AC Data Lineage now supports data lineage for correction values.

A single correction value corresponds to the value of a single field from a timeseries 
correction record in ``AC Plus``.
A new Cassandra table is required to store the correction data: ``lineage.attribute_correction``.
Data lineage for correction values can be queried the same way as done for regular values, just using
different endpoints. The endpoints are:

.. code-block:: console

	http://{host}:{port}/corrections/{objectId}/{attributeId}/{appliesAt}
	http://{host}:{port}/corrections/{objectId}/{attributeId}/{appliesAt}/{recordedOn}

Additionally, for a regular value query you can now request to include related correction values in 
the result via new query parameter ``corrections``:

.. code-block:: console

	http://{host}:{port}/{objectId}/{attributeId}/{appliesAt}/{recordedOn}?corrections=true

**Reference:** DL-118, DL-119


*****
Fixed
*****

None.


*******
Updated
*******

.. DL-71: Capture FE function call result

========================================
Include function return value in lineage
========================================

The return value of a logged FE function is now included in the data lineage.
For example:

.. code-block:: console

	...
        {
            "type" : "scope",
            "name" : "select",
            "returnValue" : {
                "status" : 1,
                "type" : "string",
                "value" : [
                    "ABC"
                ]
            },
            ...
        }
        ...

**Reference:** DL-71



