###########
1.0.4 (TBD)
###########

*******
Summary
*******

|rn-new| 1 `New`_

|rn-fixed| 0 `Fixed`_

|rn-updated| 0 `Updated`_

*************
Install Notes
*************

A new column needs to be added to the ``lineage.attribute_change`` table in Cassandra:

.. code-block:: console

   ALTER TABLE lineage.attribute_change ADD caused_by_changes text;

************
Known issues
************

==============
Authentication
==============

There is at the moment no authentication.

***
New
***

..  DL-130: Capture related ts_update and obj_update from reference data function

=================================================================
Support caused/causedBy relations between attribute value changes
=================================================================

Now supporting Data Lineage containing relations between attribute value changes as produced 
by AC Server 7.4.4.
A new text column ``caused_by_changes`` needs to be added to the Cassandra 
``attribute_change`` table:

.. code-block:: console

   ALTER TABLE lineage.attribute_change ADD caused_by_changes text;

The AC Data Lineage Read Service now possibly returns two extra JSON attributes 
in the data lineage upon a query: ``causedByChanges`` and ``causedChanges``.
Both are arrays of attribute value ID's which identify changes that caused this change (``causedByChanges``)
and changes that this change caused (``causedChanges``).

Note:: 

  Please read the Release Notes of AC Server 7.4.4 to see the exact scenarios resulting 
  in these caused/causedBy relations between attribute changes.

**Reference:** DL-130

*****
Fixed
*****

None.

*******
Updated
*******

None.


