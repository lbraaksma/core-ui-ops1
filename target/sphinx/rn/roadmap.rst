#######
Roadmap
#######

The following features will be added to the next release of |project|:

* *Showing formulas* - For derived values |project| stores the formula's that were called during the derivation 
and various information from the formulas such as the source values used and lineage log messages.
This feature will also allow the user to view the exact formula that was used to calculate a value.
As formulas can be changed over time, |project| will keep track of formula versions so that it can show the version 
of the formula that was used at the time of the calculation.

* *Validation* - |project| will keep track of the validation formulas that have been applied. This will show
how an attribute value has been validated.
